---
title: "Figure 2 - ATAC-seq examples"
author: "Andrew"
date: "2/20/2020"
output: html_document
---

```{r setup, include=FALSE}
library(miR.Enhancers.Pac)
library(miREnhPac)
library(ggplot2)
library(pheatmap)
library(tidyr)
library(dplyr)
library(stringr)
library(tibble)
library(GenomicRanges)
library(GRVis2)
library(png)
library(RColorBrewer)
library(grid)
library(reshape2)
library(ggrepel)
library(gridExtra)
library(scales)
library(cowplot)
library(data.table)
library(TxDb.Mmusculus.UCSC.mm10.knownGene)
library(edgeR)
library(limma)
library(knitr)
library(kableExtra)
knitr::opts_chunk$set(echo = FALSE,message=FALSE,warning = FALSE)

#Dims.
axis_text <- element_text(size=9,color="gray27")
axis_title<- element_text(size=9,color="black",face="italic")
labs_text_sz<- 2.5
pt_size   <- 2
ln_size   <- 0.25

#Aliases.
select    <- dplyr::select
mutate    <- dplyr::mutate
arrange   <- dplyr::arrange
filter    <- dplyr::filter
reduce    <- GenomicRanges::reduce
rename    <- dplyr::rename
melt      <- reshape2::melt
features  <- GRVis2::features
plotGeneTrack<- GRVis2::plotGeneTrack

#Directories.
dirs      <- projectDirs()$dirs
themes    <- plotThemes()
base_dir  <- file.path(dirs$figs,"miRNA_enhancer_figures","figure2")
data_dir  <- file.path(base_dir,"data")
figs_dir  <- file.path(base_dir,"pngs")

#Plot functions.
source(file.path(base_dir,"miRNA_enhancer_grange_plot_functions.R"))
```

#Figure 2-ATAC-seq specific findings
```{r loadData}
ovr_write <- TRUE
idr_gr    <- getIDRs(append_counts = "normalized")
genes     <- atacGetData("mm10_genes")
enhs      <- miR.Enhancers.Pac::fullEnhancerGR(cache_rds = file.path(data_dir,"enhancer.rds"))
exons     <- atacGetData("mm10_exons")
tads      <- miREnhPac::getTADs()
genes     <- add_gr_column(genes,query_in = tads,col_names = "name",new_cols = "TAD")
idr_gr    <- add_gr_column(idr_gr,query_in = tads,col_names = "name",new_cols = "TAD")
```
```{r plotSix2AndEnh}
p_six2_g  <- resize(genes[genes$gene_name=="Six2"],width = 7000,fix="center") %>%
              plot_script(region_name = "Six2 gene",
                          overwrite_po = ovr_write,
                          x_break_step = 2000,
                          show_y_axis_title = FALSE)
p_six2_e  <- resize(enhs[enhs$name=="enh_101249"],width = 5000,fix="center")%>%
              plot_script(region_name = "Six2 enhancer",
                          overwrite_po = ovr_write,
                          x_break_step= 2000)
ggsave(p_six2_g,filename = file.path(figs_dir,"six2_promoter.png"),units="in",device = "png",height = 10,width = 10,dpi = 300)
```
```{r plotOpeningRegion,fig.height=4,fig.width=12,eval=TRUE}
#Unknown opening region.
open_gr   <- idr_gr[idr_gr$peak_name == "peak_56696"]
p_open    <-  open_gr %>% 
              resize(width = 4000,fix = "center") %>%
              plot_script(region_name = "Opening example",
                          overwrite_po = ovr_write,
                          x_break_step = 2000)
gene_env(open_gr)
ggsave(filename = file.path(base_dir,"example_opening_TAD.png"),device = "png",units = "in",width = 12,height = 4,dpi = 300)
```
```{r plotOpeningRobo1Region,fig.height=4,fig.width=12,eval=FALSE}
#Robo1 enhancer.
open_gr   <- idr_gr[idr_gr$peak_name == "peak_54486"]
p_open    <-  open_gr %>% 
              resize(width = 5000,fix = "center") %>%
              plot_script(region_name = "Robo1 enhancer",
                          show_y_axis_title = TRUE,
                          overwrite_po = ovr_write,
                          x_break_step = 2000)
gene_env(open_gr)
ggsave(filename = file.path(base_dir,"example_opening_TAD.png"),device = "png",units = "in",width = 12,height = 4,dpi = 300)
```
```{r plotClosingRegion,fig.height=4,fig.width=12}
close_gr  <- idr_gr[idr_gr$peak_name == "peak_16070"]
p_close   <- close_gr %>% 
              resize(width = 5000,fix = "center") %>%
              plot_script(region_name = "Closing example",
                          overwrite_po = ovr_write,
                          x_break_step = 2000)
gene_env(close_gr)
ggsave(filename = file.path(base_dir,"example_closing_TAD.png"),device = "png",units = "in",width = 12,height = 4,dpi = 300)
```
```{r gdnfEnhancer}
p_gdnf_g  <- promoters(genes[genes$gene_name=="Gdnf"],upstream = 2000,downstream = 1500) %>%
              plot_script(region_name = "Gdnf gene",
                          overwrite_po = ovr_write)
p_gdnf_e  <- idr_gr[idr_gr$peak_name == "peak_50332"] %>% 
              resize(width = 3000,fix = "center") %>%
              plot_script(region_name = "Gdnf enhancer",
                          overwrite_po = ovr_write,
                          show_y_axis_title = TRUE)
```

#Final panel, no cut points.
```{r combinedFigure,fig.height=6,fig.width=7.5,echo=TRUE}
panel_label_size  <- 10
p <- grid.arrange(grobs = list(add_panel_label(p_six2_g,"A",font_size = panel_label_size),
                               add_panel_label(p_six2_e,"B",font_size = panel_label_size),
                               add_panel_label(p_open,"C",font_size = panel_label_size),
                               add_panel_label(p_gdnf_e,"D",font_size = panel_label_size),
                               add_panel_label(p_gdnf_g,"E",font_size = panel_label_size),
                               add_panel_label(p_close,"F",font_size = panel_label_size)),
                  layout_matrix=rbind(c(1,NA,2,NA,3),
                                      c(4,NA,5,NA,6)),
                  widths = c(2.2,0.1,2,0.1,2))
if(FALSE){
p <- grid.arrange(grobs = list(add_panel_label(p_open,"A",font_size = panel_label_size),
                               add_panel_label(p_six2_g,"B",font_size = panel_label_size),
                               add_panel_label(p_six2_e,"C",font_size = panel_label_size),
                               add_panel_label(p_gdnf_e,"D",font_size = panel_label_size),
                               add_panel_label(p_gdnf_g,"E",font_size = panel_label_size),
                               add_panel_label(p_close,"F",font_size = panel_label_size)),
                  layout_matrix=rbind(c(1,NA,2,NA,3),
                                      c(4,NA,5,NA,6)),
                  widths = c(2.1,0.1,2,0.1,2))
}
ggsave(p,filename=paste0(base_dir,"/figure2_full.png"),device = "png",width = 7.5,height =5.5,dpi=250,units = "in")
```

**Figure 2: Specific ATAC-seq results** IDR peaks which are stable between time points are highlighted in blue, those which exhibit increasing or decreasing accessibility are highlighted in green and red, respectively. Yellow barplots within IDR peaks represent the number of transposase insertion events that fall within each respective 25bp bin, normalized for GC content and embryo sex. Orange blocks in gene tracks indicate mm10 coordinates for features identified by CAGE in the FANTOM5 consortium mouse database. **A)** Accessibile chromatin at the promoter of the *Six2* transcription factor. **B)** Accessible chromatin at the *Six2* enhancer, location published by Park et.al in 2012. **C)** An unannotated IDR peak which increases in accessibility over time, and falls within 250kb of the promoter for miR-9. **D)** An IDR peak which decreases in accessibility oer time, and which falls within range of the promoter for the let-7c and miR-125b.
