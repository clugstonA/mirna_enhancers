---
title: "Figure 5 - Assay enhancers"
author: "Andrew"
date: "6/12/2020"
output: html_document
---

```{r setup, include=FALSE}
library(miR.Enhancers.Pac)
library(miREnhPac)
library(ggplot2)
library(pheatmap)
library(tidyr)
library(dplyr)
library(stringr)
library(tibble)
library(GenomicRanges)
library(GRVis2)
library(png)
library(RColorBrewer)
library(grid)
library(reshape2)
library(ggrepel)
library(gridExtra)
library(scales)
library(cowplot)
library(data.table)
library(TxDb.Mmusculus.UCSC.mm10.knownGene)
library(edgeR)
library(limma)
library(gtable)
library(ggplotify)
library(knitr)
library(DESeq2)
library(magrittr)
library(kableExtra)
library(biomaRt)
knitr::opts_chunk$set(echo = FALSE,message=FALSE,warning = FALSE)

#Aliases.
select    <- dplyr::select
mutate    <- dplyr::mutate
arrange   <- dplyr::arrange
filter    <- dplyr::filter
reduce    <- GenomicRanges::reduce
rename    <- dplyr::rename
melt      <- reshape2::melt
features  <- GRVis2::features
plotGeneTrack<- GRVis2::plotGeneTrack

#Directories.
dirs      <- projectDirs()$dirs
progs     <- projectDirs()$programs
themes    <- plotThemes()
base_dir  <- file.path(dirs$figs,"miRNA_enhancer_figures","figure5")
data_dir  <- file.path(base_dir,"data")
pd        <- miREnhPac::grvisParadigm(cache_file_name = file.path(data_dir,"def_paradigm.pd"),overwrite_cache = TRUE)

#Dims.
axis_text   <- element_text(size=7.6,color="gray27")
axis_title  <- element_text(size=8,color="black")
strip_text  <- element_text(size=6,color="black")
grid_line   <- element_line(size=0.25,color="darkgray")
labs_text_sz<- 2.25
pt_size     <- 1.5
```
```{r functions}
sort_tfs      <- function(tf_vector,as_index=FALSE){
  tb   <- tibble(name=tf_vector,
                num =as.integer(str_match(name,"[:digit:]+$"))) %>%
            mutate(idx=row_number()) %>%
            rowwise() %>%
            mutate(pfx=gsub(paste0(num,"$"),"",name)) %>%
            arrange(pfx,num)
  if(as_index){
    select(tb,idx) %>% unlist(use.names=FALSE) %>% return()
  }else{
    select(tb,name) %>% unlist(use.names=FALSE) %>% return()
  }
}
mini_bdg      <- function(idr_name="peak_46319",pd_in=pd,idr_alpha=0.2,bot_mar=-0.01,left_mar=-0.01){
  idr_gr      <- idrs[idrs$peak_name==idr_name]
  #idr_reg     <- resize(idr_gr,width = roundUp(place = 1000,width(idr_gr)),fix = "center")
  idr_reg     <- resize(idr_gr,width = 2500,fix = "center")
  fls_pfx     <- file.path(data_dir,idr_name)
  
  po          <- plotObject(idr_reg,plot_name = idr_name,paradigm = pd_in,overwrite_cache = FALSE,
                            cache_file = paste0(fls_pfx,".po"))
  dat_acc     <- bedGraphs(po,track_index = -7,summarize_output = FALSE)
  acc_bdg     <- lapply(names(dat_acc), function(x) mutate(dat_acc[[x]]$data,sample=x)) %>% 
                  do.call(rbind,.) %>%
                  separate(sample,into = c("age","rep"),sep = "-",remove = FALSE)
  x_ax_lab    <- paste0(prettyTitle(as.character(seqnames(idr_reg))),
                        " (",round(width(idr_reg)/1e3,digits=1),"kb)")
  x_rng       <- c(start=start(idr_reg),end=end(idr_reg))
  y_rng       <- c(min=0,max=roundUp(max(acc_bdg$score),place = 0.1))
  age_fills   <- c(E14="lightgreen",P0="darkgreen")
  acc_lab     <- acc_bdg %>%
                  group_by(sample,age,rep) %>%
                  summarize(x_pos = end(idr_reg) - 0.025 * diff(x_rng),
                            y_pos = 0.5 * diff(y_rng),.groups="drop")
  ggplot(acc_bdg,aes(x=start,y=score,fill=age)) +
    facet_wrap(.~sample,ncol=1) +
    scale_x_continuous(name=x_ax_lab,labels = comma,limits = x_rng,expand = c(0,0)) +
    scale_y_continuous(name="Pileup",breaks = max(y_rng),limits = y_rng,expand=c(0,0)) +
    scale_fill_manual(name= "Age",values = age_fills) +
    geom_hline(yintercept = 0,size=0.2,color="black") +
    annotate(geom="rect",xmin=start(idr_gr),xmax = end(idr_gr),alpha=idr_alpha,
             ymin=-Inf,ymax=Inf,fill=ifelse(idr_gr$change=="Opening","green","red")) +
    geom_area(color="black",size=0.2) +
    geom_text(data=acc_lab,mapping=aes(x=x_pos,y=y_pos,label=sample),size=labs_text_sz,inherit.aes=FALSE,hjust=1) +
    theme(strip.text=element_blank(),
          plot.margin = unit(c(0,0,bot_mar,left_mar),units="lines"),
          strip.background = element_blank(),
          panel.background = element_blank(),
          panel.grid = element_blank(),
          panel.spacing.y = unit(0,units="lines"),
          axis.title.x = element_blank(),
          axis.ticks.x = element_blank(),
          axis.text.x = element_blank(),
          axis.text.y = element_blank(),
          axis.ticks.y=element_blank(),
          axis.title.y=element_blank(),
          legend.position = "none") %>% 
    return()
}
source(file.path(base_dir,"plot_area_map.R"))
tf_label      <- function(tf_vector=tf_vec,max_char=30){
    tibble(nm = tf_vector,
           nchar = nchar(nm),
           cum_sum = cumsum(nchar),
           row_number = floor(cum_sum / max_char) + 1) %>%
      group_split(row_number) %>%
      lapply(function(x) select(x,nm) %>% 
                          unlist(use.names=FALSE) %>%
                          paste(collapse=", ")) %>%
      paste(collapse=",\n") %>% 
      gsub(",\n$","",.) %>%
      paste0(".") %>%
      return()
}
layer_features<- function(plot_region_gr=tad_gr,feature_gr=genes,color_in=NA,fill_in="purple",alpha_in=1,ymin_in=-0.4,ymax_in=-0.3){
  x_rng <- c(start(plot_region_gr),end(plot_region_gr))
  gr    <- feature_gr[queryHits(findOverlaps(feature_gr,plot_region_gr))] %>%
            reduce() %>%
            as_tibble() %>%
            rowwise() %>%
            mutate(start=max(start(plot_region_gr),start),
                   end = min(end(plot_region_gr),end))
  if(nrow(gr) > 0){
    return(geom_rect(data=gr,
                     mapping=aes(xmin=start,xmax=end),
                     ymin=ymin_in,ymax=ymax_in,
                     color=color_in,
                     fill=fill_in,
                     alpha=alpha_in,
                     inherit.aes=FALSE))
  }else{
    return(null)
  }
}
plot_levels   <- function(peak_nm = "peak_54636",tb_in = all_tb){
  tb  <- tb_in %>%
          filter(peak_name == peak_nm) %>%
          as_tibble() %>%
          select(peak_name,mir_name,
                       enh_logFC,enh_padj,
                       starts_with("enh_E14"),
                       starts_with("enh_P0"),
                       mir_log2FoldChange,mir_padj,
                       starts_with("mir_E14"),
                       starts_with("mir_P0")) %>%
          melt(id.vars=c("peak_name","mir_name")) %>%
          separate(variable,c("feature","metric"),sep = "_") %>%
          mutate(metric=gsub("\\.","-",metric),
                 metric=gsub("log2FoldChange","log2FC",metric),
                 name = ifelse(feature=="enh",
                               "insertions",
                               "thousand counts")) %>%
          group_split(feature)
  names(tb)   <- sapply(tb, function(x) unlist(select(x,feature),use.names=FALSE)[1])
  
  bar_theme <- theme(panel.background = element_blank(),
                     panel.grid = element_blank(),
                     panel.border = element_rect(size=0.5,fill=NA,color="black"),
                     strip.background = element_blank(),
                     legend.position = "none",
                     axis.text.y = axis_text,
                     axis.title.x = element_blank(),
                     axis.text.x = element_blank(),
                     axis.title.y = axis_title,
                     axis.ticks.x = element_blank(),
                     strip.text = strip_text,
                     panel.spacing.x = unit(0.25,"cm"))
  
  #Plot miRNA.
  tb_lab  <- filter(tb$mir,metric %in% c("logFC","padj"))
  tb_pts  <- tb$mir %>%
              filter(!metric %in% c("log2FC","padj")) %>%
              separate(metric,c("age","rep"),sep="-") %>%
              mutate(age=gsub("E14","E14.5",age),
                     age = factor(age,levels=c("E14.5","P0")),
                     feature = ifelse(feature=="enh","Enhancer","miRNA")) %>%
              select(-peak_name)
  
  tb_bar  <- tb_pts %>%
              group_by(age,mir_name,feature) %>%
              summarize(mean=mean(value),
                        sd = sd(value),
                        .groups="drop") %>%
              arrange(mir_name) %>%
              ungroup()
  fcts_mir<- nrow(tb_bar)
  lab_y_ps<- 0.05 * max(tb_pts$value)
  p_mir   <- ggplot(tb_bar,aes(x=age,y=mean,alpha=age,label=age)) +
              facet_wrap(.~mir_name,nrow=1,scales="fixed",strip.position = "top") +
              scale_x_discrete() +
              #scale_y_continuous(name=bquote('Counts /'~10^3),expand=expansion(mult=c(0,0.1)),label=comma,position = "right") +
              scale_y_continuous(name="TPM",expand=expansion(mult=c(0,0.1)),label=comma,position = "right") +
              scale_alpha_manual(values= c(E14=1,P0=0.5)) +
              geom_bar(stat="identity",position="dodge",color=NA,fill="chartreuse3") +
              geom_text(y=lab_y_ps,angle=0,vjust=0,hjust=0.5,alpha=1,size=labs_text_sz) +
              geom_point(data=tb_pts,mapping=aes(x=age,y=value),
                         fill="chartreuse3",pch=21,size=pt_size,inherit.aes=FALSE,alpha=1,
                         position=position_jitter(width=0.2)) +
              bar_theme
  #Manually set axis text facet to 1cm.
  gt  <- ggplot_gtable(ggplot_build(p_mir))
  gt$widths[length(gt$widths) - 3] <- unit(1,"cm")
  p_mir   <- as.ggplot(gt)
  #Plot enhancer.
  tb_pts  <- tb$enh %>%
              select(-mir_name,-feature,-name) %>%
              rename(insertions=value) %>%
              distinct() %>%
              filter(!metric %in% c("logFC","padj")) %>%
              separate(metric,into = c("age","rep"),sep = "-") %>%
              mutate(age=gsub("E14","E14.5",age))
  tb_bar  <- tb_pts %>%
              group_by(peak_name,age) %>%
              summarize(insertions=mean(insertions),.groups="drop")
  fcts_enh<- nrow(tb_bar)
  lab_y_ps<- 0.05 * max(tb_pts$insertions)
  p_enh   <- ggplot(tb_pts,aes(x=age,y=insertions,alpha=age,label=age)) +
              facet_wrap(.~peak_name) +
              scale_x_discrete() +
              scale_y_continuous(name="Insertions",expand=expansion(mult=c(0,0.1)),label=comma) +
              scale_alpha_manual(values= c(E14=1,P0=0.5)) +
              geom_bar(data=tb_bar,stat="identity",position="dodge",color=NA,fill="orange") +
              geom_text(y=lab_y_ps,angle=0,vjust=0,hjust=0.5,size=labs_text_sz,alpha=0.5) +
              geom_point(fill="orange",pch=21,size=pt_size,alpha=1,
                         position = position_jitter(width = 0.2)) + 
              bar_theme
  gt  <- ggplot_gtable(ggplot_build(p_enh))
  #gt$widths[length(gt$widths) - 3] <- unit(1,"cm")
  p_enh   <- as.ggplot(gt)
  #return(plot_grid(p_mir,p_enh,align = "v",ncol = 1,axis = "lr"))
  return(plot_grid(p_enh,p_mir,align = "h",nrow = 1,axis = "tb",rel_widths = c(fcts_enh,fcts_mir)))
}
```
```{r loadData}
genes       <- atacGetData("mm10_genes")
exons       <- atacGetData("mm10_exons")
bdg_fls     <- getBDGs()

tfs         <- getFootprints()
tads        <- getTADs()
genes       <- add_gr_column(genes,query_in = tads,col_names = "name",new_cols = "TAD")
idrs        <- getIDRs(append_counts = "normalized") %>%
                add_gr_column(query_in = tads, col_names = "name",new_cols = "TAD")
enhs        <- miR.Enhancers.Pac::fullEnhancerGR(cache_rds = file.path(data_dir,"enhancers.rds"))
mirs        <- getMirs()
#cnts        <- 2^assay(mirs$rlog)/1e3
cnts        <- mirs$tpm
cnms        <- cnts$id
cnts        <- as.matrix(select(cnts,-id))
rownames(cnts)<- cnms
mirs        <- mirs$results %>%
                mutate(name = gsub("mmu-","",name)) %>%
                makeGRangesFromDataFrame(keep.extra.columns = TRUE) %>%
                add_gr_column(query_in = tads, col_names = "name",new_cols = "TAD")

idrs_tb     <- idrs %>%
                as_tibble() %>%
                select(seqnames,start,end,peak_name,logFC,padj,change,test_miRNA,TAD,
                       starts_with("E",ignore.case = FALSE),starts_with("P",ignore.case = FALSE)) %>%
                filter(!is.na(test_miRNA)) %>%
                rowwise() %>%
                mutate(test_miRNA = str_split(test_miRNA,pattern=";")) %>%
                ungroup() %>%
                unnest(test_miRNA) %>%
                rename_all(function(x) paste0("enh_",x)) %>%
                rename(seqnames=enh_seqnames,
                       mir_name=enh_test_miRNA,
                       TAD = enh_TAD,
                       peak_name = enh_peak_name)
mirs_tb   <- mirs %>% 
              as_tibble() %>%
              merge(cnts,by.x="id",by.y="row.names") %>%
              select(seqnames,start,end,strand,name,id,log2FoldChange,padj,TAD,
                     starts_with("E",ignore.case = FALSE),
                     starts_with("P",ignore.case = FALSE)) %>%
              mutate(strand = strand,
                     name = gsub("mmu-","",name)) %>%
              filter(name %in% c(unlist(idrs_tb$mir_name)) & 
                     TAD %in% unlist(idrs_tb$TAD)) %>%
              rename_all(function(x) paste0("mir_",x)) %>%
              rename(seqnames=mir_seqnames,
                     TAD = mir_TAD)
#Check that all TADs, locations, miRNA names, and peaks are good:
# (all should be in agreement).
#merge(idrs_tb,mirs_tb,by="mir_name") %>%
#  as_tibble() %>%
#  select(peak_name,starts_with("TAD"),seqnames.x,seqnames.y)
all_tb  <- merge(idrs_tb,mirs_tb,by=c("mir_name","seqnames","TAD")) %>%
            select(peak_name,mir_name,seqnames,TAD,everything()) %>%
            rowwise() %>%
            mutate(distance = as.integer(mean(enh_start,enh_end) - mean(mir_start,mir_end))) %>%
            merge(as_tibble(tads) %>% select(start,end,name) %>% rename(tad_start=start,tad_end=end),
                  by.x = "TAD",by.y="name",all.y=FALSE) %>%
            select(peak_name,mir_name,seqnames,enh_logFC,enh_padj,mir_log2FoldChange,mir_padj,TAD,distance,everything())
pk_nms  <- unique(all_tb$peak_name)[-4]
names(pk_nms)   <- c("A","B","C")
```
```{r getTFsOfInterest}
#Get all TFs that fall in the assay regions.
tfs_oi  <- tfs[queryHits(findOverlaps(tfs,idrs[idrs$peak_name %in% pk_nms]))] %>%
            as_tibble() %>%
            select(tf,score) %>%
            mutate(tf_num=as.integer(str_match(string = tf,"[:digit:]+$"))) %>%
            rowwise() %>%
            mutate(tf_nam=gsub(paste0(tf_num,"$"),"",tf)) %>%
            arrange(tf_nam,tf_num) %>%
            select(tf,score)
#Only include TFs that are present in the tfs_oi.csv
if(FALSE){
#This is to generate an empty list of TFs when building a new tsf_oi.csv.
mart  <- useMart(biomart = "ENSEMBL_MART_ENSEMBL",dataset = "mmusculus_gene_ensembl")
atts  <- c("external_gene_name","uniprot_gn_id","go_id","name_1006","ensembl_gene_id","description")
bm    <- getBM(attributes = atts,
               filters="external_gene_name",
               values = tfs_oi$tf,
               mart = mart)
go_tb <- bm %>%
          as_tibble() %>%
          group_by(external_gene_name) %>%
          summarize(uniprot_id = uniprot_gn_id[1],
                    ensembl_id = ensembl_gene_id[1],
                    go_id = list(unique(go_id)),
                    go_name=list(unique(name_1006)),
                    description = trimws(str_extract(description[1],"^[^\\[]+")),
                    .groups="drop") %>%
          mutate(description = gsub(",","",description))

  go_tb %>%
    select(external_gene_name,description,uniprot_id) %>%
    write.table(file = file.path(base_dir,"tfs_oi.csv"),sep = ",",quote = FALSE,row.names = FALSE,col.names = TRUE)
all_gos <- tolower(unlist(unique(go_tb$go_name)))
all_gos <- all_gos[all_gos!=""]
}
gos_oi  <- file.path(base_dir,"tad_tfs_oi.csv") %>%
            fread(sep=",",header = TRUE,stringsAsFactors = FALSE) %>%
            as_tibble() %>%
            filter(reason != "")
tfs_oi  <- filter(tfs_oi,tf %in% gos_oi$external_gene_name)
#            rowwise() %>%
 #           mutate(reason = str_split(reason,pattern = ";")) %>%
  #          select(external_gene_name,reason) %>%
   #         unnest(reason) %>%
    #        mutate(reason = trimws(tolower(reason))) %>%
     #       mutate(official_term = reason %in%)
```
```{r plotMap,fig.height=10,fig.width=14}
#Inset accessibility?
# https://www.r-bloggers.com/plots-within-plots-with-ggplot2-and-ggmap/
lab_font_size = 10
end_pt_size   = 36
end_pt_adjust = 3e5
block_size    = 1
show_raw_tads = FALSE
#maxA: 3.6; maxB: 1.4; maxC: 5.7
p_maps  <- list(A=plot_area_map(peak_nm = pk_nms[2],
                                pt_sz = end_pt_size,
                                blk_sz = block_size,
                                show_raw_tad=show_raw_tads,
                                pt_adj=end_pt_adjust,
                                key_len=1e5,
                                max_tf_lab_line_chars = 33,
                                key_height = -0.9,
                                acc_bin_num = 500,
                                tfs_of_interest = tfs_oi$tf,
                                peak_lab_x_frac = 0.5,
                                peak_lab_y_coord = 0.65,
                                tf_label_offset = -100000,
                                inset_bot_mar = -0.2,
                                inset_left_mar = -0.2,
                                inset_x_min_frac = 0.68,
                                inset_x_max_frac = 0.98) %>%
                    add_panel_label(panel_label = "A",font_size = lab_font_size),
                B= plot_area_map(peak_nm = pk_nms[1],
                                pt_sz = end_pt_size,
                                blk_sz = block_size,
                                show_raw_tad=show_raw_tads,
                                pt_adj=end_pt_adjust,
                                key_len=1e5,
                                key_height = -0.9,
                                acc_bin_num = 500,
                                tfs_of_interest = tfs_oi$tf,
                                tf_label_offset = -10000,
                                peak_lab_x_frac = 0.2,
                                peak_lab_y_coord = 0.75,
                                inset_x_min_frac = 0.68,
                                inset_x_max_frac = 0.98,
                                inset_bot_mar = -0.2,
                                inset_left_mar = -0.2) %>%
                    add_panel_label(panel_label = "B",font_size = lab_font_size),
                C=plot_area_map(peak_nm = pk_nms[3],
                                pt_sz = end_pt_size,
                                blk_sz = block_size,
                                show_raw_tad=show_raw_tads,
                                pt_adj=end_pt_adjust,
                                key_len=1e5,
                                key_height = -0.9,
                                acc_bin_num = 700,
                                tfs_of_interest = tfs_oi$tf,
                                tf_label_offset = -700000,
                                inset_y_min_coord = 0.45,
                                inset_y_max_coord = 1.45,
                                inset_x_min_frac = 0.18,
                                inset_x_max_frac = 0.48,
                                inset_bot_mar = -0.2,
                                peak_lab_x_frac = 0.64,
                                peak_lab_y_coord =0.85,
                                inset_left_mar = -0.2) %>%
                    add_panel_label(panel_label = "C",font_size = lab_font_size)) %>%
            grid.arrange(grobs=.,ncol=1)

#p_bars  <- lapply(unique(all_tb$peak_name)[-4], plot_levels) %>%
p_bars  <- list(plot_levels(pk_nms[2]),
                plot_levels(pk_nms[1]),
                plot_levels(pk_nms[3])) %>%
            plot_grid(plotlist = .,align = "v",axis = "lr",ncol = 1)
p <- grid.arrange(p_maps,p_bars,ncol=2,widths=c(6.5,4.5))
ggsave(p,filename = file.path(base_dir,"figure5_full.png"),device = "png",units = "in",width = 7.5,height = 5.5,dpi = 300)
```
```{r tableReasonsForTFs}
tb  <- gos_oi %>%
        rowwise() %>%
        mutate(reason = list(unlist(str_split(reason,pattern=";")))) %>%
        unnest(reason) %>%
        mutate(reason= trimws(reason)) %>%
        group_by(external_gene_name) %>%
        mutate(shade = cur_group_id() %% 2 == 0) %>%
        mutate_at(c("external_gene_name","description","uniprot_id"),function(x) ifelse(row_number() == 1,x,"")) %>%
        rename(GO_rationale=reason)
tb %>%  
  mutate(external_gene_name = cell_spec(external_gene_name,"html",
                                        link = paste0("https://www.uniprot.org/uniprot/",uniprot_id))) %>%
  select(external_gene_name,description,GO_rationale) %>%
  rename_all(prettyTitle) %>%
  kable(format = "html",escape = FALSE,booktabs=TRUE) %>%
  kable_styling(full_width=FALSE) %>%
  column_spec(1,bold=TRUE,italic=TRUE) %>%
  row_spec(which(!tb$shade),background = "gainsboro")
    
```
```{r tadGenes}
tad_genes <- genes[genes$TAD %in% c("TAD_2284","TAD_1948","TAD_255")] 
tb  <- as_tibble(tad_genes) %>% 
        select(TAD,gene_name,gene_biotype) %>% 
        arrange(TAD,gene_biotype,gene_name) %T>%
          write.table(file = file.path(base_dir,"tad_of_interest_gene_list.tsv"),
                      sep="\t",row.names = FALSE,col.names = TRUE)
tb %>%
  group_by(TAD) %>%
  mutate(TAD=ifelse(row_number() == 1,TAD,"")) %>%
  rename_all(prettyTitle) %>%
  kable() %>%
  kable_styling(full_width=FALSE) %>%
  column_spec(1,bold=TRUE) %>%
  row_spec(which(tb$TAD != "TAD_2284"),background = "lightgray",color="black") %>%
  row_spec(which(tb$TAD == "TAD_2284"),color="black")
```

**Figure 5: smRNA-seq results. Map of tads with candidate enhancers of miRNA for *in vivo* analysis** 