---
title: "Figure 6 - *Eyes absent 1* and *Paired box protein 8* enhancers"
author: "Andrew"
date: "1/6/2021"
output: html_document
---

```{r setup, include=FALSE}
library(miR.Enhancers.Pac)
library(miREnhPac)
library(ggplot2)
library(pheatmap)
library(tidyr)
library(dplyr)
library(stringr)
library(tibble)
library(GenomicRanges)
library(GRVis2)
library(png)
library(RColorBrewer)
library(grid)
library(reshape2)
library(ggrepel)
library(gridExtra)
library(scales)
library(cowplot)
library(data.table)
library(TxDb.Mmusculus.UCSC.mm10.knownGene)
library(edgeR)
library(limma)
library(knitr)
library(kableExtra)
library(SingleCellExperiment)

knitr::opts_chunk$set(echo = FALSE,message=FALSE,warning = FALSE)

#Dims.
axis_text <- element_text(size=9,color="gray27")
axis_title<- element_text(size=9,color="black",face="italic")
labs_text_sz<- 2.5
pt_size   <- 2
ln_size   <- 0.25

#Aliases.
select    <- dplyr::select
mutate    <- dplyr::mutate
arrange   <- dplyr::arrange
filter    <- dplyr::filter
reduce    <- GenomicRanges::reduce
rename    <- dplyr::rename
melt      <- reshape2::melt
features  <- GRVis2::features
plotGeneTrack<- GRVis2::plotGeneTrack

#Directories.
dirs      <- projectDirs()$dirs
themes    <- plotThemes()
base_dir  <- file.path(dirs$figs,"miRNA_enhancer_figures","figure6")
data_dir  <- file.path(base_dir,"data")
figs_dir  <- file.path(base_dir,"pngs")
```
```{r functions}
get_count_data      <- function(gene_names = "Eya1",count_unit="logcounts",cluster_col = "Cluster3",summarize_output=FALSE){
  mtx       <- assay(wksc_dat,count_unit)
  rownames(mtx) <- tolower(rownames(mtx))
  colnames(mtx) <- colData(wksc_dat)$cell
  gene_nms  <- tolower(gene_names[tolower(gene_names) %in% rownames(mtx)])
  if(length(gene_nms) == 0){message("No genes in WKSC data.");return(NULL)}
  mtx   <- mtx[gene_nms,,drop=FALSE] %>%
            as.matrix() %>%
            as.data.frame() %>%
            rownames_to_column("gene_name") %>%
            as_tibble %>%
            pivot_longer(cols = -gene_name, names_to = "cell",values_to="counts") %>%
            pivot_wider(id_cols = cell,names_from = gene_name,values_from = counts) %>%
            merge(colData(wksc_dat)[,c("cell",cluster_col)]) %>%
            as_tibble %>%
            rename(cluster=!!as.name(cluster_col))
  levels(mtx$cluster) <-c("self-renew",
                          "primed",
                          "differentiating",
                          "proximal_tubule",
                          "distal_tubule",
                          "podocytes")
  if(summarize_output){
    mtx   <- mtx %>%
              pivot_longer(cols = -c(cell,cluster),names_to = "gene",values_to = "counts") %>%
              group_by(cluster,gene) %>% 
              summarize(sd = sd(counts),
                        cells = n(),
                        cell_frac = sum(counts > 0)/cells,
                        mean = mean(counts),
                        .groups="drop")
  }
  return(mtx)
}
get_pt_data   <- function(proj_name = "TSNE",pt_brks = 30,cluster_col = "Cluster3",pt_column = "slingPseudotime_3"){
  cbind(reducedDim(wksc_dat,proj_name),
         colData(wksc_dat)[,c("cell",cluster_col,pt_column)]) %>%
    as_tibble %>%
    rename(x=1,y=2,pt = !!as.name(pt_column),clst=!!as.name(cluster_col)) %>%
    filter(!is.na(pt)) %>%
    arrange(pt) %>%
    mutate(bin = cut(pt,breaks = pt_brks),
           bin = str_extract(bin,"[0123456789,\\.]+")) %>%
    group_by(bin) %>%
    summarize(sd_x = sd(x),
              sd_y = sd(y),
              x = mean(x),
              y = mean(y),
              .groups="drop")
}
plot_wksc_projection<- function(gene_names = c("Six2","Eya1"),count_unit="logcounts",cluster_col = "Cluster3",pt_sz = 3,proj_name = "TSNE",
                                cluster_map_facet = 1,n_cols =1,cell_colors = NULL,pt_column = "slingPseudotime_3",pt_brks = 30,pt_color = "white",
                                pt_alpha=1,lab_size=2,high_col="black",low_col="gray"){
  if(is.null(cell_colors)){
    cell_colors   <- c(`self-renew`="lightgreen",
                       primed = "darkgreen",
                       differentiating = "darkblue",
                       tubular_prox = "orange",
                       tubular_dist = "purple",
                       podocytes = "firebrick")
  }
  #If a pseudotime column is specified, gather its data and pack into line/point geoms.
  if(is.null(pt_column)){
    pt_geoms<- NULL
  }else{
    pt_dat  <- get_pt_data(proj_name = proj_name,cluster_col = cluster_col,pt_brks = pt_brks,pt_column = pt_column) %>%
                mutate(gene = "Clusters")
    pt_geoms <- geom_smooth(data=pt_dat,mapping=aes(x=x,y=y), color=pt_color,size=1,inherit.aes = FALSE,method = "loess",formula = y~x,alpha=pt_alpha)
  }
  cnts  <- get_count_data(gene_names = gene_names,count_unit=count_unit,cluster_col = cluster_col) %>%
            merge(reducedDim(wksc_dat,proj_name) %>%
                    cbind(colData(wksc_dat)[,"cell",drop=FALSE],.) %>%
                    as_tibble() %>%
                    rename(x=2,y=3)) %>%
            as_tibble() %>%
            mutate(clusters = 0) %>%
            pivot_longer(cols = -c(cell,cluster,x,y),names_to = "gene",values_to = "counts") %>%
            mutate(gene = prettyTitle(gene)) %>%
            mutate(gene = {
              levs  <- setdiff(sort(unique(gene)),"clusters")
              levs  <- setNames(seq_along(levs),nm = levs)
              ge_idx<- levs >= cluster_map_facet
              levs[ge_idx]  <- levs[ge_idx] + 1
              levs  <- c(levs,clusters=cluster_map_facet)
              factor(gene,levels=names(sort(levs)))}) %>%
            arrange(counts) #Sort by counts to plot highest expression last.
  lab_df<- cnts %>% 
            group_by(cluster) %>%
            summarize(x=mean(x),y=mean(y),.groups="drop") %>%
            mutate(gene=factor("Clusters",levels=levels(cnts$gene)))
  
  #Manually build color-coded layers of cells.
  map_cells   <- cnts %>%
    filter(gene == "Clusters") %>%
    group_split(cluster) %>%
    lapply(function(x) {
      clst  <- as.character(x$cluster[1])
      geom_point(data = x,color=cell_colors[clst],size=pt_sz)
    })
  ggplot(cnts,aes(x=x,y=y,color=counts)) +
    facet_wrap(.~gene,ncol = n_cols,strip.position = "right") +
    geom_point(size=pt_sz) +
    map_cells +
    pt_geoms +
    geom_label(data=lab_df,label.padding = unit(0.1,"lines"),
               mapping = aes(x=x,y=y,label=gsub(" ","\n",prettyTitle(as.character(cluster)))),
               inherit.aes = FALSE,size=lab_size,alpha=0.5) +
    scale_color_gradient(name = prettyTitle(count_unit),low=low_col,high=high_col) +
    labs(x = paste(proj_name,"1"),y=paste(proj_name,"2")) +
    theme(panel.background = element_blank(),
          panel.border = element_rect(size=0.5,color="blacK",fill=NA),
          strip.background = element_blank(),
          strip.text = element_text(size=15))
}
plot_tf_prints      <- function(po_in=po,isolated=FALSE,unique_piles=TRUE,inc_tfs = c("Hoxa5","Hoxd10","Barx2","Sall1","Maz"),axis_y_text_size=6,
                                highlight_tfs=NULL,highlight_color="red",highlight_dull_color="black",labs_text_sz = 2){
  tf_gr <- po_in@TrackFeatures$`TF prints`$GR
  #Remove duplicated footprints.
  #tf_gr <- tf_gr[!duplicated(tf_gr$idx)]
  if(!is.null(inc_tfs)){
    tf_gr   <- tf_gr[which(tf_gr$tf %in% inc_tfs)]
  }
            
  x_rng <- c(start=start(po_in@GenomicRange),end=end(po_in@GenomicRange))
  if(length(tf_gr) == 0){
    p   <- ggplot() +
            scale_x_continuous(limits=c(start(po_in@GenomicRange),end(po_in@GenomicRange)),labels=comma,expand=c(0,0)) +
            scale_y_continuous(name = "TF\nfootprints",limits=c(0,1),expand = c(0,0))
  }else{
    tb    <- tf_gr %>% 
              resize(0.1 * diff(x_rng),fix="center") %>%
              as_tibble %>% 
              arrange(start) %>%
              mutate(highlight_tf = tf %in% highlight_tfs,
                     x_pos = (start + end) / 2,
                     pile = 1,
                     y_pos = collapse_reads(starts=start,ends=end),
                     pile = ifelse(y_pos == 1,lag(pile) + 1,lag(pile)),
                     pile = ifelse(is.na(pile),1,pile),
                     label = ifelse(strand == "+",
                                    paste0(tf,"\u25B6"),
                                    paste0("\u25C0",tf)))
    if(unique_piles){
      tb <- tb %>%
              group_by(tf,strand,pile,highlight_tf) %>%
              summarize(count = n(),
                        x_pos = mean(x_pos),
                        .groups="drop") %>%
              mutate(start = x_pos - 0.05 * diff(x_rng),
                     end = x_pos + 0.05 * diff(x_rng),
                     y_pos = collapse_reads(starts=start,ends=end),
                     label = ifelse(strand == "+",
                                    paste0(tf,ifelse(count>1,paste0("(",count,")"),""),"\u25B6"),
                                    paste0("\u25C0",tf,ifelse(count>1,paste0("(",count,")"),""))))
    }
    p <- ggplot(tb,aes(x=x_pos,y=y_pos,shape=strand,color=highlight_tf,label=label)) +
      geom_text(data=subset(tb,strand=="+"),hjust=1,size=labs_text_sz) +
      geom_text(data=subset(tb,strand=="-"),hjust=0,size=labs_text_sz) +
      scale_x_continuous(limits=c(start(po_in@GenomicRange),end(po_in@GenomicRange)),labels=comma,expand=c(0,0)) +
      scale_y_continuous(name = "TF\nfootprints",limits = c(0.75,max(tb$y_pos)+0.25)) +
      scale_color_manual(values=c(`TRUE`=highlight_color,`FALSE`=highlight_dull_color)) +
      scale_shape_manual(values = c(`+`='\u25B6',`-`='\u25C0'))
  }
  p <- p + 
      theme(panel.background = element_blank(),
            panel.grid.major.x = element_line(size=0.5,color="gray",linetype = "dotted"),
            axis.title.y = element_text(size=axis_y_text_size,angle=0,vjust=0.5),
            axis.ticks.y = element_blank(),
            axis.text.y = element_blank(),
            legend.position = "none")
  if(!isolated){
    return(list(plot=p,plotObject = po_in))
  }else{
    return(p)
  }
}
plot_pt_expression  <- function(gene_names = "Eya1",count_unit="logcounts",cluster_col = "Cluster3",pt_sz=2,pt_col = "slingPseudotime_2",cell_colors=cell_colors,sd_alpha=0.2){
  cnts  <- get_count_data(gene_names = gene_names,count_unit=count_unit,cluster_col = cluster_col) %>%
            merge(colData(wksc_dat)[,c("cell",pt_col)]) %>%
            as_tibble %>%
            rename(pseudotime=!!as.name(pt_col)) %>%
            filter(!is.na(pseudotime)) %>%
            pivot_longer(cols = -c(cell,cluster,pseudotime),names_to="gene",values_to = "counts") %>%
            mutate(gene = prettyTitle(gene))
  avg_cnts<- cnts %>% 
              mutate(bin = cut(pseudotime,breaks = 30),
                     bin = str_extract(bin,"[0123456789,\\.]+")) %>%
              group_by(gene,bin) %>%
              summarize(sd = sd(counts),
                        counts = mean(counts),
                        .groups="drop") %>% 
              separate(bin, into = c("pt_min","pt_max"),sep=",",convert = TRUE) %>%
              mutate(pseudotime = (pt_min + pt_max) / 2)
  if(is.null(cell_colors)){
    color_scale   <- scale_color_manual(name = "Cell type")
  }else{
    color_scale   <- scale_color_manual(name = "Cell type",values = cell_colors)
  }
  ggplot(cnts,mapping= aes(x=pseudotime,y=counts,color=cluster)) +
    geom_point(size=pt_sz) + 
    geom_rect(data=avg_cnts,mapping=aes(xmin=pt_min,xmax=pt_max,ymin=ifelse(counts-sd<=0,0,counts-sd),ymax=counts+sd),inherit.aes=FALSE,
              fill="black",alpha=sd_alpha,color=NA) +
    geom_segment(data=avg_cnts,mapping=aes(x=pt_min,xend=pt_max,y=counts,yend=counts),inherit.aes=FALSE,color="black") +
    scale_x_continuous(name = "Pseudotime",expand = c(0,0),position = "top") +
    scale_y_continuous(name = prettyTitle(count_unit)) +
    color_scale +
    facet_wrap(.~gene,ncol = 1,strip.position = "right",scales = "free_y") +
    theme(panel.background = element_blank(),
          panel.border = element_rect(size=0.5,color="black",fill=NA),
          panel.grid.major = element_line(size=0.2,color="lightgray"),
          strip.background = element_blank(),
          strip.text.y = element_text(size=8,angle=0,hjust=0),
          legend.title = element_blank(),
          legend.text = element_text(size=14))
}
plot_genome_map     <- function(gene_name_in = "Pax8",zoomed_window=NULL,inc_idrs = "specified",
                                inc_enhs = "specified",other_enhancers=NULL,other_genes=genes,window_size = 1e6,
                                x_brks = NULL,scale_pos_y=-0.7,scale_txt_pos_y = -0.5,scale_length=50000,
                                scale_length_text = "50kb",lab_text_size=1.5){
  #Get gene info.
  gr_gen  <- genes[which(genes$gene_name == gene_name_in)]
  gr_gen$feat <- "gene_footprint"
  gr_gen$lab  <- gene_name_in
  tss_coord   <- start(promoters(gr_gen,upstream = 0,downstream = 0))
  #Get gene exons.
  gr_exs  <- exons[which(exons$gene_name == gene_name_in)] %>% reduce_gr_annotated()
  gr_exs$feat <- "exons"
  #Get zoomed window info.
  if(is.null(zoomed_window)){
    zoomed_window   <- gr_enh
    gr_win  <- resize(gr_enh,window_size,fix="center")
  }else{
    gr_win  <- resize(zoomed_window,window_size,fix="center")
  }
  #Get enhancer info.
  gr_enh  <- ea2_gr[which(ea2_gr$gene_name == gene_name_in)] %>% reduce_gr_annotated()
  if(inc_enhs == "specified"){
    gr_enh<- gr_enh[queryHits(findOverlaps(gr_enh,zoomed_window))]
  }
  #Get other enhancers.
  if(!is.null(other_enhancers)){
    gr_otr_enhs <- other_enhancers[queryHits(findOverlaps(other_enhancers,gr_win))]
    gr_enh      <- c(gr_enh,gr_otr_enhs)
  }
  gr_enh$feat <- "enhancer"
  #Get other genes.
  gr_otr_genes  <- other_genes[queryHits(findOverlaps(other_genes,gr_win))]
  gr_otr_genes  <- gr_otr_genes[gr_otr_genes$gene_name != gene_name_in]
  if(length(gr_otr_genes) > 0){
    gr_otr_genes$feat <- "other_genes"
    gr_otr_genes$lab  <- gr_otr_genes$gene_name
  }
  #Get IDRs.
  gr_idrs <- idr_gr[queryHits(findOverlaps(query = idr_gr,subject = gr_win))] %>%
              reduce_gr_annotated(collapse_function = "first")
  gr_idrs$feat <- "idr_stable"
  gr_idrs$feat[gr_idrs$change == "Opening"] <- "idr_open"
  gr_idrs$feat[gr_idrs$change == "Closing"] <- "idr_close"
  if(inc_idrs == "specified"){
    gr_idrs   <- gr_idrs[queryHits(findOverlaps(gr_idrs,zoomed_window))]
  }else if(inc_idrs == "changing"){
    gr_idrs   <- gr_idrs[gr_idrs$change != "Stable"]
  }
  
  #Plot things.
  x_rng <- c(start(gr_win),end(gr_win))
  scale_pos_x <- x_rng[1] + 0.01*diff(x_rng)
  tb  <- c(gr_gen,gr_enh,gr_otr_genes,gr_idrs,gr_exs) %>%
          as_tibble() %>%
          select(feat,lab,seqnames,start,end,strand,gene_name,TAD,prediction_score,cell_type) %>%
          mutate(y = case_when(feat=="gene_footprint" ~ 0.05,
                               feat=="other_genes" ~ -0.04,
                               feat=="eya1_exon" ~ 0.15,
                               feat=="enhancer" ~ 0.1,
                               grepl("^idr",feat) ~ 1)) %>%
          arrange(y) %>%
          mutate(feat = factor(feat,levels=select(.,feat) %>% unlist(use.names=FALSE) %>% unique),
                 start = ifelse(start < x_rng[1],x_rng[1],start),
                 end = ifelse(end > x_rng[2],x_rng[2],end))
  
  if(!is.null(x_brks)){
    x_scale <- scale_x_continuous(labels = comma,limits = x_rng,expand=c(0,0),breaks = x_brks)
  }else{
    x_scale <- scale_x_continuous(labels = comma,limits = x_rng,expand=c(0,0))
  }
  
  #Enhancer-TSS curves.
  enh_curves<- tb %>% 
    filter(feat == "enhancer") %>%
    mutate(tss = tss_coord,
           dir = ifelse(tss_coord > start,
                        "downstream",
                        "upstream"),
           x_pos=(start+end)/2) %>%
    mutate(curve_ang = ifelse(dir=="downstream",
                              50,-50)) %>%
    group_split(dir) %>%
    lapply(function(x) {
      if(x$dir == "upstream"){
        geom_curve(data=x,
                   mapping=aes(x=x_pos,xend=tss,y=0,yend=0),
                   angle=x$curve_ang[1],
                   size =0.3,
                   color="black",alpha=0.8,
                   show.legend = FALSE)
      }else{
        geom_curve(data=x,
                   mapping=aes(x=tss,xend=x_pos,y=0,yend=0),
                   angle=x$curve_ang[1],
                   size =0.3,
                   color="black",alpha=0.8,
                   show.legend = FALSE)
      }})
      
  ggplot(tb,aes(xmin=start,xmax=end,x = (start + end)/2,
                ymin=-y,ymax =y, y = y,fill=feat,alpha=feat,
                label = lab)) +
    x_scale +
    scale_y_continuous(limits=c(-1,1),expand = c(0,0)) +
    scale_fill_manual(values=c(gene_footprint = "darkgreen",
                               other_genes = "black",
                               ea2_enhancer = "firebrick",
                               eya1_exon = "darkgreen",
                               idr_stable = "blue",
                               idr_open = "green",
                               idr_close = "red")) +
    scale_alpha_manual(values=c(gene_footprint = 1,
                                other_genes = 1,
                                ea2_enhancer = 1,
                                eya1_exon = 1,
                                idr_stable = 1,
                                idr_open = 1,
                                idr_close= 1)) +
    geom_hline(yintercept = 0,color="black",size=0.5) +
    enh_curves +
    annotate(geom = "rect",xmin = start(zoomed_window),xmax = end(zoomed_window),ymin = -0.3,ymax = 0.3,color="red",fill=NA,alpha=0.4,size=0.2) +
    geom_segment(data = filter(tb,grepl("gene",feat)),
                 mapping = aes(x = ifelse(strand == "+",start,end),
                               xend=ifelse(strand == "+",start,end),
                               y = -0.2,yend=0.04),
                 color="black",size=0.5,alpha=1,
                 show.legend = FALSE) +
    geom_segment(data = filter(tb,grepl("gene",feat)),
                 mapping = aes(xend= ifelse(strand == "+",start,end),
                               x=ifelse(strand == "+",start + 0.02 * diff(x_rng),end - 0.02 * diff(x_rng)),
                               y = -0.2,yend=-0.2),
                 color="black",size=0.5,alpha=1,
                 arrow = arrow(length=unit(0.05,"cm"), ends="first", type = "closed"),
                 show.legend = FALSE) +
    geom_rect(data = filter(tb,!grepl("idr",feat)),show.legend = FALSE) +
    geom_text_repel(data = filter(tb,!is.na(lab) & lab == gene_name_in),color="black",nudge_y = 0.6,show.legend = FALSE,size=lab_text_size+1.5,alpha=1,
                    mapping = aes(x=end),segment.size = 0.3) +
    geom_text_repel(data = filter(tb,!is.na(lab) & lab != gene_name_in),segment.size = 0.3,color="black",nudge_y = -0.75,show.legend = FALSE,size=lab_text_size,alpha=1) +
    geom_point(data = filter(tb, feat == "idr_stable"),y=0,pch = 23,size=1.5) +
    geom_point(data = filter(tb, feat %in% c("idr_open","idr_close")),y=0,pch = 23,size=2) +
    annotate(geom="text",x = x_rng[1],y=0.9,hjust=0,vjust=1,size=2,color="black",label=gsub("chr","Chr.",as.character(seqnames(gr_win))),alpha=1) +
    annotate(geom="segment",x = scale_pos_x,xend=scale_pos_x+scale_length,y=scale_pos_y,yend=scale_pos_y,size=1,color="black") +
    annotate(geom="text",x = scale_pos_x,y=scale_txt_pos_y,label=scale_length_text,vjust=0,hjust=0,size=2) +
    theme(panel.background = element_blank(),
          panel.border = element_rect(size=0.5,color="black",fill=NA),
          panel.grid.major.x = element_line(size=0.5,color="lightgray",linetype = "dotted"),
          panel.grid = element_blank(),
          plot.background = element_blank(),
          plot.margin = unit(units = "line",c(0.5,0,0,0)),
          axis.title.y = element_blank(),
          axis.text.y = element_blank(),
          axis.title.x = element_blank(),
          axis.ticks.y = element_blank(),
          legend.position = "none")
}
#plt_gns   <- genes[!grepl("^Gm",genes$gene_name) & genes$gene_biotype == "protein_coding"]
#plot_genome_map(gene_name = "Pax8",inc_idrs = "specified",other_enhancers = ea2_gr[ea2_gr$gene_name == "Pax8"], other_genes = plt_gns,zoomed_window = po@GenomicRange,window_size = 2.5e5)
```
```{r loadData}
ovr_write <- TRUE
idr_gr    <- getIDRs(append_counts = "normalized",cache_file = file.path(data_dir,"idrs.rds"))
names(idr_gr) <- idr_gr$peak_name
genes     <- atacGetData("mm10_genes")
exons     <- atacGetData("mm10_exons")
tads      <- miREnhPac::getTADs()
genes     <- add_gr_column(genes,query_in = tads,col_names = "name",new_cols = "TAD")
idr_gr    <- add_gr_column(idr_gr,query_in = tads,col_names = "name",new_cols = "TAD")
ea2_gr    <- getEnhancerAtlasPredictions()
ea2_gr    <- ea2_gr[ea2_gr$gene_name %in% c("Pax8","Eya1")] %>% reduce_gr_annotated()
tfs_gr    <- getFootprints()
tfs_gr$tf <- gsub("^Hx","Hox",tfs_gr$tf)
nuc_gr    <- getNucleosomes()
nfr_gr    <- GRangesList(nuc_gr$NFRs[c("E14","P0")])
nuc_gr    <- GRangesList(nuc_gr$Nucleosomes[c("E14","P0")]) %>% resize(width = 146,fix="center")
wksc_dat  <- readRDS(file.path(dirs$raw,"wksc_data/sce_np_annotated.rds"))
pd_file   <- file.path(data_dir,"plotParadigm.pd")
if(file.exists(pd_file)){
  pd  <- miREnhPac::grvisParadigm(cache_file_name = pd_file) %>%
          features(track_index = c("Other motifs","TF motifs"),remove_tracks = TRUE) %>%
          features(featGRList = ea2_gr,featNames = "EA2 enhancers",
                   set_values = list(y_range_min = -0.3,
                                    y_range_max = 0.3,
                                    fill="firebrick",
                                    color="black")) %>%
          features(featGRList = tfs_gr,featNames = "TF prints",
                   set_values = list(y_range_min =-0.25,
                                     y_range_max = 0.25,
                                     fill="lightgray",
                                     color="black",
                                     lab_col = "tf",
                                     lab_y_min = -0.9,
                                     lab_y_max=-0.7)) %>%
          features(featGRList = nfr_gr,featNames = c("E14 NFRs","P0 NFRs"),
                   set_values = list(y_range_min = -0.8,
                                     y_range_max = 0.8,
                                     fill="red",
                                     color="black")) %>%
          features(featGRList = nuc_gr,featNames = c("E14 Nucleosomes","P0 Nucleosomes"),
                   set_values = list(y_range_min = -1,
                                     y_range_max = 1,
                                     fill="black",
                                     color="black")) %>%
          features(track_index = "Protein",set_values = list(alpha=1,y_range_min = -0.25,y_range_max = 0.25,lab_size=1.75))
  saveRDS(pd,pd_file)
}else{
  pd  <- readRDS(pd_file)
}

#Published/specific enhancers
##Eya1 Park 2012:
# Paper: chr1:14,625,471-14,626,271 (Cons. region on UCSC GB).
# Lifted: chr1:14635390-14636190
specific_paper_enhs<- c(GRanges(seqnames = "chr1",ranges = IRanges(start=14635390,end=14636190))) #Park, Eya1
```

#Figure 6: *Eyes absent 1 (Eya1)* and *Paired box protein 8* enhancer details
##Enhancer region, [Eya1](https://www.uniprot.org/uniprot/P97767).
```{r plotEnhancerRegionEya1,fig.height=7,fig.width=5}
gr  <- GRanges(seqnames = "chr1",ranges = IRanges(14216500,14221000))
po_file   <- file.path(data_dir,paste0("eya1_enh.po"))
po  <- plotObject(GR=gr,plot_name = "Eya1 enhancer",paradigm = pd,cache_file = po_file)
y_ax_ttl_size <- 5
x_axis_scale  <- scale_x_continuous(breaks = 14217000 +c(0,1500,3000),labels=comma,expand=c(0,0),limits = c(start(gr),end(gr)))
p_bdg <- plotBedGraphPanel(po,bedGraph_index = c(1:6),
                           y_axis_breaks = 3,
                           y_axis_drop_zero = TRUE,
                           facet_label_size = 3,
                           y_axis_size = 7,
                           isolated = FALSE) %>%
          highlightsLayer(background_layer = TRUE,uniquify = TRUE) %>%
          .$plot +
          theme(axis.text.x = element_blank(),
                axis.title.x = element_blank(),
                axis.ticks.x = element_blank(),
                axis.title.y = element_text(size= y_ax_ttl_size),
                plot.title = element_blank(),
                plot.margin = unit(c(0,1,-0.15,1),"line")) +
            x_axis_scale
#p_bdg
p_cons<- plotBedGraphPanel(po,bedGraph_index = "Conservation",
                           isolated = FALSE,
                           y_axis_limits = c(0,1),facet_label_include = FALSE) %>%
          highlightsLayer(background_layer = TRUE,uniquify = TRUE) %>%
          .$plot +
          theme(plot.title = element_blank(),
                axis.title.y = element_text(size=y_ax_ttl_size),
                panel.border = element_blank(),
                axis.line.x = element_line(size=0.5,color="black"),
                axis.text.x = element_text(size=5),
                axis.title.x = element_text(size=6),
                axis.line.y.left = element_line(size=0.5,color="black")) +
            x_axis_scale
#p_cons
p_gt  <- plotGeneTrack(po,isolated = FALSE,featTrackIndex = c(1:8),x_axis_line = FALSE) %>%
          highlightsLayer(background_layer = TRUE,uniquify = TRUE) %>%
          .$plot +
          theme(plot.title = element_blank(),
                panel.border = element_blank(),
                axis.title.x = element_blank(),
                axis.text.x = element_blank(),
                axis.ticks.x = element_blank(),
                axis.title.y = element_blank(),
                legend.position = "none",
                plot.margin = unit(c(0,1,-0.2,1),"line")) +
            x_axis_scale
#p_gt
p_tfs <- plot_tf_prints(po_in = po,labs_text_sz = 2) %>% 
          highlightsLayer(uniquify = TRUE) %>%
          .$plot +
          theme(axis.text.x = element_blank(),
                axis.ticks.x = element_blank(),
                axis.title.x = element_blank(),
                axis.title.y = element_text(size=y_ax_ttl_size),
                plot.margin = unit(c(0,1,-0.2,1),"line")) +
            x_axis_scale
#p_tfs
nuc_theme <- theme(plot.title = element_blank(),
                     panel.border = element_blank(),
                     axis.line.x = element_blank(),
                     axis.ticks = element_blank(),
                     axis.text.x = element_blank(),
                     axis.text.y = element_blank(),
                     axis.title.x = element_blank(),
                     axis.title.y = element_text(angle=0,hjust=0.5,vjust=0.5,size=y_ax_ttl_size),
                     plot.margin = unit(c(0,1,-0.2,1),"line"),
                     legend.position = "none")
p_nuc14 <- plotGeneTrack(po,isolated=FALSE,featTrackIndex = c(10,12),y_axis_label = "E14.5 nucs.",x_axis_line = FALSE) %>%
            highlightsLayer(background_layer = TRUE,uniquify = TRUE) %>%
            .$plot + nuc_theme + x_axis_scale
p_nucP0 <- plotGeneTrack(po,isolated=FALSE,featTrackIndex = c(11,13),y_axis_label = "P0 nucs.",x_axis_line = FALSE) %>%
            highlightsLayer(background_layer = TRUE,uniquify = TRUE) %>%
            .$plot + nuc_theme + x_axis_scale
#Genome map.
p_map   <- plot_genome_map(gene_name_in = "Eya1",
                           window_size = 1e6,
                           scale_pos_y = 0.2,
                           scale_txt_pos_y = 0.3,
                           scale_length = 100000,
                           scale_length_text = "100kb",
                           x_brks = c(14e6,14.5e6),
                           inc_idrs = "specified",
                           other_enhancers = specific_paper_enhs,
                           other_genes = genes[genes$gene_name != "Eya1" & !grepl("^Gm",genes$gene_name)],
                           zoomed_window = po@GenomicRange) +
              theme(axis.text.x = element_text(size=5))
#p_map
#p_tfs <- p_tfs + annotation_custom(grob = ggplotGrob(p_inset),xmin = 14219000,xmax = 14221000)
p_eya1 <- cowplot::plot_grid(p_map,p_bdg,p_nuc14,p_nucP0,p_tfs,p_gt,p_cons,align = "v",axis = "lr",ncol = 1,rel_heights = c(2,6,0.3,0.3,0.6,1,2))

panel_label_size  <- 10
p_eya1 <- add_panel_label(p_eya1,"A",font_size = panel_label_size) %>%
          add_panel_label("B",font_size = panel_label_size,y_pos = -5)
#grid.arrange(p_enh)
ggsave(p_eya1,filename = file.path(base_dir,"eya1_enh.png"),device = "png",units = "in",dpi = 300,height = 7,width = 5)
plot(p_eya1)
```

##Enhancer region, [Pax8](https://www.uniprot.org/uniprot/Q00288).
```{r plotEnhancerRegionPax8,fig.height=7,fig.width=5}
gr  <- resize(idr_gr[idr_gr$peak_name == "peak_5562"],width=4500,fix="center")
x_axis_scale  <- scale_x_continuous(breaks = 24416000 + c(0,2000),labels=comma,expand=c(0,0),limits = c(start(gr),end(gr)))
po_file   <- file.path(data_dir,paste0("pax8_enh.po"))
po  <- plotObject(GR=gr,plot_name = "Pax8 enhancer",paradigm = pd,cache_file = po_file)
y_ax_ttl_size <- 5
p_bdg <- plotBedGraphPanel(po,bedGraph_index = c(1:6),
                           y_axis_breaks = 3,
                           y_axis_drop_zero = TRUE,
                           facet_label_size = 3,
                           y_axis_size = 7,
                           isolated = FALSE) %>%
          highlightsLayer(background_layer = TRUE,uniquify = TRUE) %>%
          .$plot +
          theme(axis.text.x = element_blank(),
                axis.title.x = element_blank(),
                axis.ticks.x = element_blank(),
                axis.title.y = element_blank(),
                plot.title = element_blank(),
                plot.margin = unit(c(0,1,-0.15,1),"line")) +
          x_axis_scale
#p_bdg
p_cons<- plotBedGraphPanel(po,bedGraph_index = "Conservation",
                           isolated = FALSE,
                           y_axis_limits = c(0,1),facet_label_include = FALSE) %>%
          highlightsLayer(background_layer = TRUE,uniquify = TRUE) %>%
          .$plot +
          theme(plot.title = element_blank(),
                panel.border = element_blank(),
                axis.line.x = element_line(size=0.5,color="black"),
                axis.text.x = element_text(size=5),
                axis.title.x = element_text(size=6),
                axis.title.y =  element_blank(),
                axis.line.y.left = element_line(size=0.5,color="black")) +
          x_axis_scale
#p_cons
p_gt  <- plotGeneTrack(po,isolated = FALSE,featTrackIndex = c(1:8),x_axis_line = FALSE) %>%
          highlightsLayer(background_layer = TRUE,uniquify = TRUE) %>%
          .$plot +
          theme(plot.title = element_blank(),
                panel.border = element_blank(),
                axis.title.x = element_blank(),
                axis.text.x = element_blank(),
                axis.ticks.x = element_blank(),
                axis.title.y = element_blank(),
                legend.position = "none",
                plot.margin = unit(c(0,1,-0.2,1),"line")) +
          x_axis_scale
#p_gt
p_tfs <- plot_tf_prints(po_in = po,labs_text_sz = 2) %>% 
          highlightsLayer(uniquify = TRUE) %>%
          .$plot +
          theme(axis.text.x = element_blank(),
                axis.ticks.x = element_blank(),
                axis.title.x = element_blank(),
                axis.title.y = element_blank(),
                plot.margin = unit(c(0,1,-0.2,1),"line")) +
          x_axis_scale
#p_tfs
nuc_theme <- theme(plot.title = element_blank(),
                     panel.border = element_blank(),
                     axis.line.x = element_blank(),
                     axis.ticks = element_blank(),
                     axis.text.x = element_blank(),
                     axis.text.y = element_blank(),
                     axis.title.x = element_blank(),
                     axis.title.y = element_blank(),
                     #axis.title.y = element_text(angle=0,hjust=0.5,vjust=0.5,size=y_ax_ttl_size),
                     plot.margin = unit(c(0,1,-0.2,1),"line"),
                     legend.position = "none")
p_nuc14 <- plotGeneTrack(po,isolated=FALSE,featTrackIndex = c(10,12),y_axis_label = "E14.5 nucs.",x_axis_line = FALSE) %>%
            highlightsLayer(background_layer = TRUE,uniquify = TRUE) %>%
            .$plot + nuc_theme + theme(axis.title.y = element_blank()) +
            x_axis_scale
p_nucP0 <- plotGeneTrack(po,isolated=FALSE,featTrackIndex = c(11,13),y_axis_label = "P0 nucs.",x_axis_line = FALSE) %>%
            highlightsLayer(background_layer = TRUE,uniquify = TRUE) %>%
            .$plot + nuc_theme + theme(axis.title.y = element_blank()) +
            x_axis_scale
#Genome map.
p_map   <- plot_genome_map(gene_name_in = "Pax8",
                inc_idrs = "specified",
                #other_enhancers = ea2_gr[ea2_gr$gene_name == "Pax8"],
                other_genes = genes[!grepl("^Gm",genes$gene_name)],
                zoomed_window = po@GenomicRange,
                window_size = 2.5e5,
                scale_pos_y = 0.2,
                scale_txt_pos_y = 0.3,
                x_brks = c(24350000,24500000),
                scale_length = 25000,
                scale_length_text = "25kb") +
            theme(axis.text.x = element_text(size=5))
#p_map
#p_tfs <- p_tfs + annotation_custom(grob = ggplotGrob(p_inset),xmin = 14219000,xmax = 14221000)
p_pax8  <- cowplot::plot_grid(p_map,p_bdg,p_nuc14,p_nucP0,p_tfs,p_gt,p_cons,align = "v",axis = "lr",ncol = 1,rel_heights = c(2,6,0.3,0.3,0.6,1,2))

panel_label_size  <- 10
p_pax8  <- add_panel_label(p_pax8,"C",font_size = panel_label_size) %>%
           add_panel_label("D",font_size = panel_label_size,y_pos = -5)
#grid.arrange(p_enh)
ggsave(p_pax8,filename = file.path(base_dir,"pax8_enh.png"),device = "png",units = "in",dpi = 300,height = 7,width = 5)
plot(p_pax8)
```

##Gene expression
```{r plotGeneExp,fig.height = 7,fig.width=5}
exp_pan_theme <- theme(legend.position = "bottom",
                  strip.text.y = element_text(size=6,angle=0,face = "italic"),
                  axis.text = element_text(size=6),
                  axis.title = element_text(size=7),
                  legend.text = element_text(size=6),
                  legend.title = element_text(size=7,vjust=0.9),
                  legend.key.height = unit(0.3,"cm"),
                  plot.margin = unit(c(0,0,0.5,0),"lines"))
cell_colors   <- c(`self-renew`="chartreuse",
                   primed = "chartreuse4",
                   differentiating = "aquamarine3",
                   proximal_tubule = "orange",
                   distal_tubule = "purple",
                   podocytes = "firebrick")
p_proj  <- plot_wksc_projection(c("Eya1","Pax8","Six2"),n_cols = 1,cluster_map_facet = 2,pt_sz = 0.25,cell_colors = cell_colors,
                                pt_column = "slingPseudotime_3",lab_size = 1.5,pt_color = "black",pt_brks = 30,pt_alpha=0.6,high_col="blue") +
            exp_pan_theme
#p_proj
p_pt    <- plot_pt_expression(gene_names = c("Eya1","Pax8","Six2"),pt_col = "slingPseudotime_3",pt_sz=0.25,cell_colors = cell_colors) + 
            labs(y="Log(transcripts)") +
            exp_pan_theme +
            theme(legend.position = "none")
p_expr  <- cowplot::plot_grid(p_pt,p_proj,align = "v",axis = "lr",rel_heights = c(2,6),ncol=1) %>%
            add_panel_label("E",font_size = panel_label_size,y_pos = -0.15)
ggsave(p_expr,filename = file.path(base_dir,"gene_expr.png"),device = "png",units = "in",height = 7,width = 5,dpi = 300)
#plot_pt_expression(gene_names = c("Eya1","Six2"),pt_col = "slingPseudotime_1")
#plot_pt_expression(gene_names = c("Eya1","Six2"),pt_col = "slingPseudotime_2")
#plot_pt_expression(gene_names = c("Eya1","Six2"),pt_col = "slingPseudotime_3")
```
```{r finalPlot}
p <- grid.arrange(grobs = list(p_eya1,p_pax8,p_expr),
                  layout_matrix=rbind(c(1,2,3)),
                  widths = c(5,4.5,4.5))
ggsave(p,filename = file.path(base_dir,"figure6_full.png"),device = "png",height = 5,width = 7.5,dpi = 300,units = "in")
```
```{r parkEya1Enhancer,eval=FALSE}
enhs <- fullEnhancerGR(cache_rds = file.path(data_dir,"enhancers.rds")))
```
```{r observeMazPrints}
tfs_gr[queryHits(findOverlaps(tfs_gr,idr_gr[idr_gr$peak_name == "peak_5562"]))] %>%
  as_tibble %>%
  select(start,end,strand,width,tf,score) %>% 
  filter(tf == "Maz") %>%
  mutate(range = max(end) - min(start))

```

