#!/usr/bin/env nextflow

/*Directory structure.*/
params.dir_base="/projects/andrew/miRNA_enhancers"
params.dir_raw=params.dir_base + "/raw/atac_seq"
/*params.dir_raw=params.dir_base + "/raw/atac_seq_sub"*/
params.dir_src=params.dir_base + "/src/atacseq_pipeline"
params.dir_prc=params.dir_base + "/prc/atac_seq"
dir_alg=params.dir_prc + "/aligned"
dir_sig=params.dir_prc + "/signal"
dir_pek=params.dir_prc + "/peaks"
dir_lap=dir_pek + "/overlaps"
dir_idr=dir_pek + "/idr"

/*Programs.*/
params.trimGalore_exe="/data/opt/bio/TrimGalore-0.4.3/trim_galore"
params.bowtie_exe="/data/opt/bio/bowtie2-2.3.1/bowtie2"
params.samtools_exe="/data/opt/bio/samtools-1.3.1/bin/samtools"
params.picard_exe="/data/opt/bio/picard-tools-v2.10.9/picard.jar"
params.bedtools_exe="/data/opt/bio/bedtools-2.26.0/bin/bedtools"
params.macs2_exe="/data/opt/bio/anaconda2/bin/macs2"
params.ucsc="/data/opt/bio/ucsc_kent_v343"
params.idr_exe="~/anaconda3/bin/idr"

/*Scripts*/
params.assignMulti=params.dir_src + "/assign_multimappers.py"
params.compileOverlapsScript=params.dir_src + "/compile_peak_replicates.R"

/*Parameters.*/
/*params.rawReads=params.dir_raw + "/*_R{1,2}_sub.fastq.gz"*/
params.rawReads=params.dir_raw + "/*_R{1,2}.fastq.gz"
params.genome="/projects/annotation/genomes/mm10/Sequence/Bowtie2Index/genome"
params.genome_sizes="/data/projects/annotation/genomes/mm10/Sequence/mm10_chromSizes.txt"
params.samcores=4
params.blackListFile=params.dir_raw + "/blacklist/mm10.blacklist.v2.bed"
params.matePairDist=2000
params.bowtieThreads=16
params.multiMapping=4
params.subBedPEReads=10000000
params.sampPfxs=["E14","E16","P0"]
params.idrThreshold=0.1

/*Input value channels*/
Channel
	.fromFilePairs(params.rawReads)
	.ifEmpty { error "No paired reads found." }
	.set { read_pairs_ch }

Channel
	.from(params.sampPfxs)
	.into { age_pool_ch_1;
			age_pool_ch_2;
			age_pool_ch_3;
			age_pool_ch_4 }

log.info """\

Trimming and alignment.
=======================================
"""

process trimFastQs {
	tag "${pair_id}"
	publishDir "$dir_alg/$pair_id", mode: 'copy', pattern: "*.trm.log"
	
	input:
	set pair_id, file(reads) from read_pairs_ch

	output:
	set val("${pair_id}"), val("${age}"), file("${pair_id}_R1*.fq.gz"), file("${pair_id}_R2*.fq.gz") into trim_pair_ch
	stdout results
	
	script:
	age=pair_id.split("-")[0]
	
	"""
	t1=${reads[0]}
	t1_pfx=\${t1%%.fastq.gz}
	t2=${reads[1]}
	t2_pfx=\${t2%%.fastq.gz}
	
	$params.trimGalore_exe --paired $reads

	"""
}

process alignReads {
	tag "${pair_id}"
	publishDir "$dir_alg/$pair_id", mode: 'copy', pattern: "*.txt"
	publishDir "$dir_alg/$pair_id", mode: 'copy', pattern: "*.log"

	input:
	set pair_id, age, file("input_R1.fq.gz"), file("input_R2.fq.gz") from trim_pair_ch

	output:
	set val("${pair_id}"), val("${age}"), file("${pair_id}.raw.sam") into raw_sam_ch
	file("${pair_id}.bowtie2.metrics.txt")
	file("${pair_id}.bowtie2.log")

	"""
	$params.bowtie_exe	\
				--threads $params.bowtieThreads \
				-q \
				-X $params.matePairDist \
				--local \
				--mm \
				--met-file ${pair_id}.bowtie2.metrics.txt \
				-x $params.genome \
				-1 input_R1.fq.gz \
				-2 input_R2.fq.gz \
				-S ${pair_id}.raw.sam 2>> ${pair_id}.bowtie2.log
	"""
}
process convertSam {
	tag "${pair_id}"

	input:
	set pair_id, age, file("raw_aligned.sam") from raw_sam_ch

	output:
	set val("${pair_id}"), val("${age}"), file("${pair_id}.raw.bam") into raw_bam_ch
	
	"""
	$params.samtools_exe view -Su raw_aligned.sam | \
	$params.samtools_exe sort -o ${pair_id}.raw.bam -
	"""
}
process rawComplexity {
	tag "${pair_id}"
	publishDir "$dir_alg/$pair_id", mode: 'copy', pattern: "*.tsv"
	publishDir "$dir_alg/$pair_id", mode: 'copy', pattern: "*.txt"
	
	input:
	set pair_id, age, file("raw.bam") from raw_bam_ch

	output:
	set val("${pair_id}"), val("${age}"), file("${pair_id}.nmsrt.raw.bam"),
		stdout into raw_namesorted_bam_ch
	file("${pair_id}_complexity.raw.tsv")
	file("${pair_id}.raw.flagstat.txt")

	"""
	$params.samtools_exe sort -n -@ $params.samcores raw.bam > ${pair_id}.nmsrt.raw.bam

	$params.bedtools_exe bamtobed -bedpe -i ${pair_id}.nmsrt.raw.bam | \
		awk 'BEGIN{OFS="\t"}{print \$1,\$2,\$4,\$6,\$9,\$10}' | \
		grep -v 'chrM' | \
		sort | \
		uniq -c | \
		awk 'BEGIN{mt=0;m0=0;m1=0;m2=0} (\$1==1){m1=m1+1} (\$1==2){m2=m2+1} {m0=m0+1} {mt=mt+\$1} END{printf "%d\\t%d\\t%d\\t%d\\t%f\\t%f\\t%f\\n",mt,m0,m1,m2,m0/mt,m1/m0,m1/m2}' > ${pair_id}_complexity.raw.tsv
	
	#Get raw flagstat output.
	$params.samtools_exe flagstat ${pair_id}.nmsrt.raw.bam > ${pair_id}.raw.flagstat.txt
	
	#Append number of lines in BAM file via stdout; doesn't seem to be a way to append numbers from BASH back into Groovy layer.
	$params.samtools_exe view -c ${pair_id}.nmsrt.raw.bam
	"""
}
process filterOneUnmapped {
	tag "${pair_id}"
	publishDir "${dir_alg}/${pair_id}", mode: 'copy', pattern: "*.txt"

	input:
	set pair_id, age, file("nmsrted.raw.bam"),
		raw_reads from raw_namesorted_bam_ch

	output:
	set val("${pair_id}"), val("${age}"), file("${pair_id}.flt1.bam"),
		val("${raw_reads}"), stdout into flt1_bam_ch
	"""
	$params.samtools_exe view -F 524 -@ $params.samcores -f 2 -u nmsrted.raw.bam > ${pair_id}.flt1.bam
	
	#Append read counts in bam post-filter.
	$params.samtools_exe view -c ${pair_id}.flt1.bam
	"""
}
process filterTwoMultimap {
	tag "${pair_id}"

	input:
	set pair_id, age, file("flt1.bam"), 
		raw_reads, flt1_reads from flt1_bam_ch

	output:
	set val("${pair_id}"), val("${age}"), file("${pair_id}.flt2.bam"),
		val("${raw_reads}"), val("${flt1_reads}"), stdout into flt2_bam_ch

	"""
	$params.samtools_exe view -h -@ $params.samcores flt1.bam | \
	$params.assignMulti -k $params.multiMapping --paired-end | \
	$params.samtools_exe view -b -@ $params.samcores - > ${pair_id}.flt2.bam	
	
	#Append read counts in bam post-filter.
	$params.samtools_exe view -c ${pair_id}.flt2.bam
	"""
}
process filterThreeOrphans {
	tag "${pair_id}"

	input:
	set pair_id, age, file("flt2.bam"),
		raw_reads, flt1_reads, flt2_reads from flt2_bam_ch
		
	output:
	set val("${pair_id}"), val("${age}"), file("${pair_id}.flt3.bam"),
		val("${raw_reads}"), val("${flt1_reads}"), val("${flt2_reads}"), stdout into flt3_bam_ch

	"""
	$params.samtools_exe fixmate -r flt2.bam flt2.fixmate.bam
	$params.samtools_exe view -F 1804 -f 2 -@ $params.samcores -u flt2.fixmate.bam | \
	$params.samtools_exe sort - -@ $params.samcores -o ${pair_id}.flt3.bam
	
	#Append read counts in bam post-filter.
	$params.samtools_exe view -c ${pair_id}.flt3.bam
	"""
}
process filterFourMitochondrial {
	tag "${pair_id}"

	input:
	set pair_id, age, file("flt3.bam"),
		raw_reads, flt1_reads, flt2_reads, flt3_reads from flt3_bam_ch

	output:
	set val("${pair_id}"), val("${age}"), file("${pair_id}.flt4.bam"),
		val("${raw_reads}"), val("${flt1_reads}"), val("${flt2_reads}"), val("${flt3_reads}"), stdout into flt4_bam_ch

	"""
	$params.samtools_exe index flt3.bam
	$params.samtools_exe idxstats flt3.bam > samp_idxstats.txt
	cut -f 1 samp_idxstats.txt | grep -v M | xargs $params.samtools_exe view -b -@ $params.samcores flt3.bam > ${pair_id}.flt4.bam
	
	#Append read counts in bam post-filter.
	$params.samtools_exe view -c ${pair_id}.flt4.bam
	"""
}
process filterFiveDuplicates {
	tag "${pair_id}"
	publishDir "$dir_alg/${pair_id}", mode: 'copy', pattern: '*.txt'

	input:
	set pair_id, age, file("flt4.bam"),
		raw_reads, flt1_reads, flt2_reads, flt3_reads, flt4_reads from flt4_bam_ch

	output:
	set val("${pair_id}"), val("${age}"), file("${pair_id}.flt.bam"),
		val("${raw_reads}"), val("${flt1_reads}"), val("${flt2_reads}"), val("${flt3_reads}"), val("${flt4_reads}"), stdout into flt_bam_ch

	"""
	java -jar $params.picard_exe MarkDuplicates \
	I=flt4.bam \
	O=flt5.bam \
	M=${pair_id}.dupMetrics.txt \
	REMOVE_DUPLICATES=false

	$params.samtools_exe view -F 1804 -f 2 -@ $params.samcores -b flt5.bam | \
	$params.samtools_exe sort - -@ $params.samcores > ${pair_id}.flt.bam
	$params.samtools_exe index ${pair_id}.flt.bam
	
	#Append read counts in bam post-filter.
	$params.samtools_exe view -c ${pair_id}.flt.bam
	"""
}
process filteredComplexity {
	tag "${pair_id}"
	publishDir "$dir_alg/$pair_id", mode: 'copy', pattern: "*.tsv"
	publishDir "$dir_alg/$pair_id", mode: 'copy', pattern: "*.log"
	publishDir "$dir_alg/$pair_id", mode: 'copy', pattern: "*.txt"
	publishDir "$dir_alg/$pair_id", mode: 'copy', pattern: "*.bam"
	
	input:
	set pair_id, age, file("flt.bam"),
		raw_reads, flt1_reads, flt2_reads, flt3_reads, flt4_reads, flt5_reads from flt_bam_ch

	output:
	set val("${pair_id}"), val("${age}"), file("${pair_id}.nmsrt.flt.bam") into flt_bam_nmsrt_ch
	file("${pair_id}.flt.flagstat.txt")
	file("${pair_id}_complexity.flt.tsv")
	file("${pair_id}.filter.log")
	

	"""
	$params.samtools_exe sort -n -@ $params.samcores flt.bam > ${pair_id}.nmsrt.flt.bam
	
	$params.bedtools_exe bamtobed -bedpe -i ${pair_id}.nmsrt.flt.bam | \
		awk 'BEGIN{OFS="\\t"}{print \$1,\$2,\$4,\$6,\$9,\$10}' | \
		grep -v 'chrM' | \
		sort | \
		uniq -c | \
		#awk 'BEGIN{mt=0;m0=0;m1=0;m2=0} (\$1==1){m1=m1+1} (\$1==2){m2=m2+1} {m0=m0+1} {mt=mt+\$1} END{printf "%d\\t%d\\t%d\\t%d\\t%f\\t%f\\t%f\\n",mt,m0,m1,m2,m0/mt,m1/m0,m1/m2}' > ${pair_id}_complexity.flt.tsv
		awk 'BEGIN{mt=1;m0=1;m1=1;m2=1} (\$1==1){m1=m1+1} (\$1==2){m2=m2+1} {m0=m0+1} {mt=mt+\$1} END{printf "%d\\t%d\\t%d\\t%d\\t%f\\t%f\\t%f\\n",mt,m0,m1,m2,m0/mt,m1/m0,m1/m2}' > ${pair_id}_complexity.flt.tsv
		
	#Write filter log csv.
	flt_log=${pair_id}.filter.log
	echo "filter,reads"					>  \$flt_log
	echo "raw,${raw_reads}"				>> \$flt_log
	echo "quality,${flt1_reads}"		>> \$flt_log
	echo "multimapped,${flt2_reads}"	>> \$flt_log
	echo "orphaned,${flt3_reads}"		>> \$flt_log
	echo "mitochondrial,${flt4_reads}" 	>> \$flt_log
	echo "duplicates,${flt5_reads}"		>> \$flt_log
	
	#Get flagstat output for post-filtered bam.
	$params.samtools_exe flagstat ${pair_id}.nmsrt.flt.bam > ${pair_id}.flt.flagstat.txt
	"""
}
process generateBedFile {
	tag "${pair_id}"

	input:
	set pair_id, age, file("flt.nmsrt.bam") from flt_bam_nmsrt_ch

	output:
	set val("${pair_id}"), val("${age}"), file("${pair_id}.bedPE") into bed_pe_ch

	"""
	$params.bedtools_exe bamtobed -bedpe -mate1 -i flt.nmsrt.bam > ${pair_id}.bedPE	
	"""
}
process tn5ShiftBedFiles {
	tag "${pair_id}"

	input:
	set pair_id, age, file("original.bedPE") from bed_pe_ch

	output:
	set val("${pair_id}"), val("${age}"), file("${pair_id}.bedPE") into tn5_shifted_bed_1_ch
	set val("${pair_id}"), val("${age}"), file("${pair_id}.bedPE") into tn5_shifted_bed_2_ch
	set val("${pair_id}"), val("${age}"), file("${pair_id}.bedPE") into tn5_shifted_bed_3_ch

	"""
	cat original.bedPE | \
	awk -v OFS="\t" '{if(\$9 == "+"){print \$1,\$2 + 4,\$3 + 4,\$4,\$5 - 5,\$6 - 5,\$7,\$8,\$9,\$10}else if(\$9 == "-"){print \$1,\$2-5,\$3-5,\$4,\$5+4,\$6+4,\$7,\$8,\$9,\$10}}' > ${pair_id}.bedPE
	"""
}
process callBroadPeaks {
	tag "${pair_id}"
	publishDir "${dir_pek}/${pair_id}", mode: 'copy', pattern: "*.gz"
	publishDir "${dir_pek}/${pair_id}", mode: 'copy', pattern: "*.log"

	input:
	set pair_id, age, file("shifted.bedPE") from tn5_shifted_bed_1_ch

	output:
	set val("${pair_id}"), val("${age}"), file("${pair_id}.bedPE") into call_bp_bedpe_ch
	file("${pair_id}.broadPeak.gz") into broad_peaks_1_ch
	file("${pair_id}.broadPeak.gz") into broad_peaks_2_ch
	set val("${pair_id}"), file("${pair_id}.gappedPeak.gz") into gapped_peaks_ch
	set val("${pair_id}"), file("${pair_id}.macs2.broadPeak.log")

	"""
	log_file=./${pair_id}.macs2.broadPeak.log
	#Apparently Macs2 doesn't accept the Bedtools BedPE format, so only feed it a three-column file: seq, start frag 1, end frag 2.
	cat shifted.bedPE | awk '{if(\$2 < \$6){print \$1"\\t"\$2"\\t"\$6}else if(\$6 < \$2){print \$1"\\t"\$6"\\t"\$2}}' > ${pair_id}.bedPE
	$params.macs2_exe callpeak \
						--treatment ${pair_id}.bedPE \
						--name ${pair_id} \
						--format BEDPE \
						--g mm \
						-p 0.01 \
						--broad \
						--shift 37 \
						--extsize 75 \
						--broad \
						--keep-dup all \
						--nomodel |& tee -a \$log_file
	#Sort broadPeak by column 8 (p-value), descending order of confidence (ENCODE).
	sort -k 8gr,8gr "${pair_id}_peaks.broadPeak" |\
		awk 'BEGIN{OFS="\\t"}{\$4="peak_"NR ; print \$0}' | \
		gzip -c > ${pair_id}.broadPeak.gz
	rm -f ${pair_id}_peaks.broadPeak
	
	#Sort gappedPeak by column 14 (-log10 p-value), descending order of confidence (ENCODE).
	sort -k 14gr,14gr "${pair_id}_peaks.gappedPeak" | \
		awk 'BEGIN{OFS="\\t"}{\$4="peak_"NR ; print \$0}' | \
		gzip -c > ${pair_id}.gappedPeak.gz
	rm -f {pair_id}_peaks.gappedPeak
	"""
}
process callNarrowPeaks {
	tag "${pair_id}"
	publishDir "${dir_pek}/${pair_id}", mode: 'copy'
	
	input:
	set pair_id, age, file("shifted.bedPE") from tn5_shifted_bed_2_ch
	
	output:
	file("${pair_id}.narrowPeak.gz") into narrowPeak_1_ch
	file("${pair_id}.narrowPeak.gz") into narrowPeak_2_ch
	set val("${pair_id}"), file("${pair_id}_control_lambda.bdg"), file("${pair_id}_treat_pileup.bdg") into call_np_treat_bdg_ch

	"""
	log_file=./${pair_id}.macs2.narrowPeak.log
	$params.macs2_exe callpeak \
							--treatment shifted.bedPE \
							--name ${pair_id} \
							--format BEDPE \
							--g mm \
							-p 0.01 \
							--shift 37 \
							--extsize 75 \
							-B \
							--SPMR \
							--keep-dup all \
							--call-summits \
							--nomodel |& tee -a \$log_file
	#Sort by column 8 in descending order of confidence and rename peaks.
	sort -k 8gr,8gr ${pair_id}_peaks.narrowPeak | \
		awk 'BEGIN{OFS="\\t"}{\$4="peak_"NR ; print \$0}' | \
		gzip -c > ${pair_id}.narrowPeak.gz
	mv ${pair_id}"_summits.bed" ${pair_id}"_narrow_summits.bed"
	rm -f ${pair_id}"_peaks.narrowPeak"
	"""
}
process generateSignalTracks {
	tag "${pair_id}"
	publishDir "${dir_pek}/${pair_id}", mode: 'copy'

	input:
	set file("shifted.bedPE") from tn5_shifted_bed_3_ch
	set pair_id, file("control.bdg"), file("treatment.bdg") from call_np_treat_bdg_ch

	output:
	file("${pair_id}_FE.bdg")
	file("${pair_id}_FE.bigWig")
	file("${pair_id}_macs2_FE.log")
	/*
	*file("${pair_id}_PS.bdg")
	*file("${pair_id}_PS.bigWig")
	*file("${pair_id}_macs2_PS.log")
	*/

	"""
	#Fold enrichment signal#
	fe_bdg_file=${pair_id}"_FE.bdg"
	fe_bw_file=${pair_id}"_FE.bigWig"
	fe_log_file=${pair_id}"_macs2_FE.log"
	$params.macs2_exe	bdgcmp \
						-t treatment.bdg \
						-c control.bdg \
						-m FE \
						--o-prefix ${pair_id} |& tee -a \$fe_log_file

	$params.bedtools_exe slop	-i \$fe_bdg_file \
								-g $params.genome_sizes \
								-b 0 | \
	${params.ucsc}/bedClip stdin ${params.genome_sizes} \$fe_bdg_file".tmp"
	sort -k1,1 -k2,2n \$fe_bdg_file".tmp" > \$fe_bdg_file
	#rm \$fe_bdg_file".tmp"
	
	${params.ucsc}/bedGraphToBigWig \$fe_bdg_file ${params.genome_sizes} \$fe_bw_file

	#Get tag count per million from initial bedPE file.
	tag_count=\$(wc -l <(cat shifted.bedPE) | awk '{printf "%f",\$1/1000000}')

	#Poisson pileup#
	ps_bdg_file=${pair_id}"_PS.bdg"
	ps_bw_file=${pair_id}"_PS.bigWig"
	ps_log_file=${pair_id}"_macs2_PS.log"
	
	#Divide by zero error when running Poisson test, try again with larger data sets (up to now 100000 reads per sample is too few).
	#$params.macs2_exe	bdgcmp \
	#					-t treatment.bdg \
	#					-c control.bdg \
	#					--o-prefix ${pair_id} \
	#					-m ppois \
	#					-S \$tag_count |& tee -a \$ps_log_file
	#mv ${pair_id}"_ppois.bdg" \$ps_bdg_file
	#$params.bedtools_exe slop	-i \$ps_bdg_file \
	#							-g $params.genome_sizes \
	#							-b 0 | \
	#$params.ucsc/bedClip stdin $params.genome_sizes \$ps_bdg_file".tmp"
	#sort -k1,1 -k2,2n \$ps_bdg_file".tmp" > \$ps_bdg_file
	#rm -f \$fe_bdg_file".tmp"
	#$params.ucsc/bedGraphToBigWig \$ps_bdg_file $params.genome_sizes \$ps_bw_file
	"""
}
process getBroadPeakOverlaps {
	tag "${age}"
	publishDir "${dir_lap}", mode: 'copy'

	input:
	val age from age_pool_ch_1
	file broadPeaks from broad_peaks_1_ch.collect()

	output:
	val("${age}") into bp_olaps_age_ch
	file("*.bed")

	script:
	reps_peaks=broadPeaks.grep(~/${age}-\d\.broadPeak.gz/).join(" ")
	
	"""
	rep_names=\$(for rep in ${reps_peaks}; do name=\$(basename \$rep); name=\${name%%.broadPeak.gz}; echo \$name; done)
	zcat ${reps_peaks} |
	$params.bedtools_exe intersect -wo \
							-a stdin \
							-b ${reps_peaks} \
							-names \$rep_names \
							-f 0.5 -F 0.5 \
							-e > ${age}"_bioReps_broadPeak_overlaps.bed"
	#Rscript ${params.compileOverlapsScript} ${age}"_bioReps_broadPeak_overlaps.bed" ${age}"_bioReps_broadPeak"
	#rm ${age}"_bioReps_broadPeak_overlaps.bed"
	"""
}
process getNarrowPeakOverlaps {
	tag "${age}"
	publishDir "${dir_lap}", mode: 'copy'

	input:
	val age from age_pool_ch_2
	file narrowPeaks from narrowPeak_1_ch.collect()

	output:
	val("${age}") into np_olaps_age_ch
	file("*.bed")

	script:
	reps_peaks=narrowPeaks.grep(~/${age}-\d\.narrowPeak\.gz/).join(" ")
	
	"""
	rep_names=\$(for rep in ${reps_peaks}; do name=\$(basename \$rep); name=\${name%%.narrowPeak.gz}; echo \$name; done)
	zcat ${reps_peaks} |
	$params.bedtools_exe intersect	-wo \
							-a stdin \
							-b ${reps_peaks} \
							-names \$rep_names \
							-f 0.5 -F 0.5 \
							-e > ${age}"_bioReps_narrowPeak_overlaps.bed"
	#Rscript ${params.compileOverlapsScript} ${age}"_bioReps_narrowPeak_overlaps.bed" ${age}"_bioReps_narrowPeak"
	#rm ${age}"_bioReps_narrowPeak_overlaps.bed"
	"""
}
process getBroadPeakIDRs {
	tag "${age}"
	publishDir "${dir_idr}/${age}_broadPeak", mode: 'copy'

	input:
	val age from age_pool_ch_3
	file broadPeaks from broad_peaks_2_ch.collect()

	output:
	val("${age}") into broadPeak_idr_age_ch
	file("*")

	script:
	rep_peaks=broadPeaks.grep(~/${age}-\d\.broadPeak\.gz/).join(" ")
	
	"""
	IFS=" " read -r -a reps <<< "${rep_peaks}"
	function idr_script {
		peak1=\$1
		peak2=\$2
		pool=\$3
		thrsh=\$4
		pk1_nm=\$(basename \$peak1)
		pk1_nm=\${pk1_nm%%.broadPeak.gz}
		pk2_nm=\$(basename \$peak2)
		pk2_nm=\${pk2_nm%%.broadPeak.gz}
		outpt_name=\$pk1_nm"_vs_"\$pk2_nm"_"\$thrsh"idr"
		$params.idr_exe	--samples \$peak1 \$peak2 \
						--peak-list \$pool \
						--input-file-type broadPeak \
						--output-file \$outpt_name.broadPeak \
						--rank p.value \
						--soft-idr-threshold \$thrsh \
						--plot \
						--log-output-file \$outpt_name.log \
						--verbose \
						--output-file-type bed
		#Filter by IDR.
		idr_val=\$(echo "-l(\$thrsh)/l(10)" | bc -l)
		awk 'BEGIN{OFS="\\t"} \$8>='"\${idr_val}"' {print \$1,\$2,\$3,\$4,\$5,\$6,\$7,\$8,\$9,\$10,\$11,\$12,\$13,\$14}' \$outpt_name.broadPeak | sort | uniq | sort -k7n,7n > \$outpt_name.broadPeak.tmp

		#Filter blacklist sites.
		$params.bedtools_exe intersect -v -a \$outpt_name.broadPeak.tmp -b $params.blackListFile | \
			awk 'BEGIN{OFS="\\t"}{if (\$5>1000) \$5=1000; print \$0}' | \
			grep -P 'chr[0-9XY]+(?!_)' | \
			gzip -nc > \$outpt_name.broadPeak.gz
		rm -f \$outpt_name.broadPeak \$outpt_name.broadPeak.tmp
	}

	#For now assume three inputs...figure out permutations later.
	zcat ${rep_peaks} | gzip -c > pooled_peaks.bed.gz
	idr_script \${reps[0]} \${reps[1]} pooled_peaks.bed.gz ${params.idrThreshold}
	idr_script \${reps[0]} \${reps[2]} pooled_peaks.bed.gz ${params.idrThreshold}
	idr_script \${reps[1]} \${reps[2]} pooled_peaks.bed.gz ${params.idrThreshold}
	rm pooled_peaks.bed.gz
	"""
}
process getNarrowPeakIDRs {
	tag "${age}"
	publishDir "${dir_idr}/${age}_narrowPeak", mode: 'copy'

	input:
	val age from age_pool_ch_4
	file narrowPeaks from narrowPeak_2_ch.collect()

	output:
	file("*")

	script:
	rep_peaks=narrowPeaks.grep(~/${age}-\d\.narrowPeak\.gz/).join(" ")

	"""
	zcat ${rep_peaks} | gzip -c > pooled_peaks.bed.gz
	IFS=" " read -r -a reps <<< "${rep_peaks}"
	function idr_script {
		peak1=\$1
		peak2=\$2
		pool=\$3
		thrsh=\$4
		pk1_nm=\$(basename \$peak1)
		pk1_nm=\${pk1_nm%%.narrowPeak.gz}
		pk2_nm=\$(basename \$peak2)
		pk2_nm=\${pk2_nm%%.narrowPeak.gz}
		outpt_name=\$pk1_nm"_vs_"\$pk2_nm"_"\$thrsh"idr"

		$params.idr_exe	--samples \$peak1 \$peak2 \
						--peak-list \$pool \
						--input-file-type narrowPeak \
						--output-file \$outpt_name.narrowPeak \
						--rank p.value \
						--soft-idr-threshold \$thrsh \
						--plot \
						--log-output-file \$outpt_name.log \
						--verbose \
						--output-file-type bed
		idr_val=\$(echo "-l(\$thrsh)/l(10)" | bc -l)
		awk 'BEGIN{OFS="\\t"} \$8>='"\${idr_val}"' {print \$1,\$2,\$3,\$4,\$5,\$6,\$7,\$8,\$9,\$10,\$11,\$12,\$13,\$14}' \$outpt_name.narrowPeak | sort | uniq | sort -k8n,8n > \$outpt_name.narrowPeak.tmp

		#Filter blacklist sites.
		$params.bedtools_exe intersect -v -a \$outpt_name.narrowPeak.tmp -b $params.blackListFile | \
			awk 'BEGIN{OFS="\\t"}{if (\$5>1000) \$5=1000; print \$0}' | \
			grep -P 'chr[0-9XY]+(?!_)' | \
			gzip -nc > \$outpt_name.narrowPeak.gz
		rm -f \$outpt_name.narrowPeak \$outpt_name.narrowPeak.tmp
	}

	#For now assume three inputs...figure out permutations later.
	idr_script \${reps[0]} \${reps[1]} pooled_peaks.bed.gz ${params.idrThreshold}
	idr_script \${reps[0]} \${reps[2]} pooled_peaks.bed.gz ${params.idrThreshold}
	idr_script \${reps[1]} \${reps[2]} pooled_peaks.bed.gz ${params.idrThreshold}
	
	rm pooled_peaks.bed.gz
	"""
}