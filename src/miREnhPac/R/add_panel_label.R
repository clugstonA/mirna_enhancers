#' add_panel_label
#'
#' Function returns a grob with a label annotated to the top left. Useful for
#' adding figure letter to plots for construction of full figure.
#'
#' Thanks to https://stackoverflow.com/questions/17576381/corner-labels-in-ggplot2
#'
#' @param plot_object GGplot object to be labeled.
#' @param panel_label Label to be appended to top-left corner.
#' @param font_size Defaults 25.

add_panel_label <- function(plot_object,panel_label="A",font_size=25,x_pos=0,y_pos=1){
  return(arrangeGrob(plot_object,
                     top = textGrob(panel_label,
                                    x = unit(x_pos,"npc"),
                                    y = unit(y_pos,"npc"),
                                    just = c("left","top"),
                                    gp = gpar(col="black",
                                              fontsize = font_size,
                                              fontface = "bold"))))
}
