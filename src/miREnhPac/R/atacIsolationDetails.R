#' getIsolationDetails
#'
#' Get ATAC-seq sample prep details from mice through library prep. Returns a large table which pulls fold-enrichment of qPCR markers from qPCR output
#' files using pcrNormalizePlate and other isolation characteristics (digestion times, dates, cell counts, library concentrations, etc.) from each sample's
#' Isolation summary file.
#'
#' getIsolationDetails()
#'
#' @import stringr
#' @import dplyr
#' @import data.table
#' @import lubridate
#'
#' @export

getIsolationDetails   <- function(only_key_cols=TRUE){
  dirs        <- projectDirs()$dirs
  base_dir    <- dirs$isolation
  fl_tb       <- tibble(file=Sys.glob(file.path(dirs$isolation,"CD1*/qPCR/* Quantification Cq Results_0.csv"))) %>%
                  mutate(sample = gsub("_","-",str_match(pattern="CD1_([EP][:digit:]{1,2}_[:digit:])_.+$",string = file)[,2])) %>%
                  group_by(sample) %>%
                  mutate(plate=row_number()) %>%
                  ungroup() %>%
                  separate(sample,into=c("age","replicate"),sep = "-",remove = FALSE) %>%
                  mutate(short_name = paste(sample,plate,sep="_")) %>%
                  select(short_name,sample,age,replicate,plate,file)
  fls         <- vectify(fl_tb,value_col = "file",name_col = "short_name")
  exp_tb      <- lapply(names(fls), function(x) pcrNormalizePlate(fls[[x]]) %>% mutate(plate=x)) %>%
                  do.call(rbind,.) %>%
                  filter(Target != "Gapdh") %>%
                  select(Target,rel_exp,nd,plate) %>%
                  mutate(sample=gsub("_[0123456789]$","",plate),
                         rel_exp = ifelse(nd=="Not detected",0,rel_exp)) %>%
                  group_by(Target,sample) %>%
                  summarize(rel_exp=rel_exp,.groups="drop") %>%
                  pivot_wider(id_cols = Target,names_from=sample,values_from=rel_exp) %>%
                  t_tib(colnames_from = "Target",rownames_col = "Sample") %>%
                  separate(Sample,into = c("age","replicate"),sep = "-",remove = FALSE)

  #Incorporate other details from each isolation.
  #1. Build a table of isolation details.
  essential_cols  <- c("base_name","base_date","base_bred","base_age","base_embryos",
                       "base_kidneys","MACS_date","MACS_digestion_time","MACS_preMACS_count",
                       "MACS_postMACS_count","MACS_tRNA_con","ATACseq_date",
                       "ATACseq_transposition_time","ATACseq_indices","ATACseq_total_cycles",
                       "ATACseq_lib_con","ATACseq_qc_seq_date","ATACseq_qc_lib_size")
  summ_tables               <- Sys.glob(paste0(dirs$isolation,"/CD1*/*_iso_summary.csv"))
  names(summ_tables)        <- gsub("_","-",str_match(pattern="/CD1_([EP][:digit:]{1,2}_[:digit:])_*",string=summ_tables)[,2])
  tb  <- lapply(names(summ_tables), function(x)
            fread(summ_tables[[x]],header=FALSE,stringsAsFactors = FALSE) %>%
              as_tibble() %>%
              mutate(Sample=x)) %>%
          do.call(rbind,.) %>%
          dplyr::rename(procedure=V1,id=V2,value=V3,data_type=V4) %>%
          mutate(id = paste(procedure,id,sep="_")) %>%
          select(-procedure)
  if(only_key_cols){
    tb<- filter(tb,id %in% essential_cols)
  }
  pivot_wider(tb,id_cols = Sample,names_from=id,values_from=value) %>%
    merge(exp_tb) %>%
    merge(select(getSexGenotypes(),Sample,female_frac)) %>%
    as_tibble() %>%
    select(Sample,age,replicate,female_frac,everything()) %>%
    return()
}

