#' getCellMarkers
#'
#' Function returns table of nephrogenic cell markers from Magella et al 2017.
#'
#' getCellMarkers()
#'
#' @export

getCellMarkers <- function(){
  read.table(file.path(projectDirs()$dirs$data,"cell_markers.csv"),sep = ",",header=TRUE,stringsAsFactors = TRUE) %>%
    as_tibble()
}
