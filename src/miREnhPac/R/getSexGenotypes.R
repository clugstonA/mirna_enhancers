#' getSexGenotypes
#'
#' Function returns table of male and female embryos from genotyping.
#'
#' getSexGenotypes()
#'
#' @export

getSexGenotypes  <- function(){
  fread(file.path(projectDirs()$dirs$raw,"sex_genotyping.csv"),sep = ",",header=TRUE,stringsAsFactors = TRUE) %>%
    select(-starts_with("V",ignore.case = FALSE)) %>%
    pivot_longer(names_to = "Sample",cols = everything()) %>%
    group_by(Sample) %>%
    summarize(female=sum(value=="F"),
              male = sum(value=="M"),
              female_frac=female/n(),
              .groups="drop") %>%
    return()
}
